<?php

namespace App\DB\Engines;

use PDO;
use PDOException;
use App\DB\DB;

class MySQLDB extends DB
{
    var $engine = "mysql";
    /**
     * @return array
     */
    public function connect()
    {
        try {
            $conectionString = $this->engine . ":host=" . $this->host . ";dbname=" . $this->dbName;
            $this->connection = new PDO($conectionString, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4'"));
            return array('success' => true, 'message' => 'success');
        } catch (PDOException $e) {
            $this->connection = null;
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    public function beginTransaction()
    {
        $this->connection->beginTransaction();
    }

    public function commit()
    {
        $this->connection->commit();
    }

    public function rollback()
    {
        $this->connection->rollback();
    }

    public function num_rows(string $query)
    {
        $affected = $this->connection->exec($query);
        if ($affected === false) {
            $err = $this->connection->errorInfo();
            if ($err[0] === '00000' || $err[0] === '01000') {
                return true;
            }
        }
        return $affected;
    }

    public function describeTable(string $table)
    {
        $sql = 'DESCRIBE ' . $table;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $campos = $stmt->fetchAll();
        $map = [];
        foreach ($campos as $campo) {
            array_push($map, $this->fk($table, $campo));
        }
        return $map;
    }

    public function raw(string $query, array $values = []): array
    {
        $result = array();
        $parts = explode("\n", $query);
        $parts = array_map(function ($part) {
            return trim($part);
        }, $parts);

        $query = implode(' ', $parts);

        try {
            $statement = $this->connection->prepare($query);
            if ($statement->execute($values)) {
                $result['success'] = true;
                $result['data'] = $statement->fetchAll(PDO::FETCH_ASSOC);
                $result['message'] = 'Data Retrieved';
            } else {
                $result['success'] = false;
                $result['data'] = [];
                $result['message'] = "Error in query '{$query}'";
            }
        } catch (PDOException  $e) {
            $result['success'] = false;
            $result['data'] = [];
            $result['message'] = $e->getMessage();
        }
        return $result;
    }

    public function getColumns(string $table, array $conditions = array(), array $columns = array(), string $operator = 'AND'): array
    {

        if ($operator !== 'AND' && $operator !== 'OR') {
            return ['success' => false, 'message' => 'Only AND / OR operators', 'data' => []];
        }

        $arrayValue = [];
        $conditionsObjs = [];

        if (empty($conditions)) {
            array_push($conditionsObjs, "1");
        } else {
            foreach ($conditions as $col => $val) {
                array_push($conditionsObjs, $col . "=? ");
                array_push($arrayValue, $val);
            }
        }

        $columnList = !empty($columns) ? implode(', ', $columns) : '*';

        $query = sprintf(
            'SELECT %s FROM  %s WHERE %s',
            $columnList,
            $table,
            implode(" $operator ", $conditionsObjs)
        );

        // Prepare PDO statement
        try {
            $statement = $this->connection->prepare($query);
        } catch (PDOException  $e) {
            return ['success' => false, 'message' => $e->getMessage(), 'data' => []];
        }

        try {
            // if (!$this->connection->inTransaction()) {
            //     $this->connection->beginTransaction();
            // }
            if ($statement->execute($arrayValue)) {
                // if (!$this->connection->inTransaction()) {
                //     $this->connection->commit();
                // }
                $data = $statement->fetchAll(PDO::FETCH_ASSOC);
                // foreach ($data as &$dat) {
                //     foreach ($dat as &$columns) {
                //         $columns = utf8_encode($columns);
                //     }
                // }
                return ['success' => true, 'message' => 'Data Retrieved', 'data' => $data];
            } else {
                // $this->connection->rollback();
                return ['success' => false, 'message' => 'Data Not Retrieved', 'data' => []];
            }
        } catch (PDOException $e) {
            // $this->connection->rollback();
            return ['success' => false, 'message' => $e->getMessage(), 'data' => []];
        }
    }

    /**
     * @param string $table
     * @param array $rows
     * @param array $columns
     * @return mixed
     */
    public function insert(string $table, array $rows, array $columns, bool $withTransaction = true): array
    {
        if (empty($rows)) {
            return ['success' => false, 'errores' => ['empty_rows']];
        }
        if (empty($columns)) {
            return ['success' => false, 'errores' => ['empty_columns']];
        }

        foreach ($rows as $index => $rowData) {
            if (gettype($rowData) != "array") {
                $data = array($rows);
                $rows = $data;
                break;
            }
        }
        $errores = array();
        foreach ($rows as $index => $rowData) {
            $keys = array_keys($rowData);
            if (!$this->array_equal($columns, $keys)) {
                array_push($errores, array('index' => $index, 'error' => $rowData));
            }
        }

        if (!empty($errores)) {
            return array('mensaje' => 'columnas no existen', 'errores' => $errores, 'success' => false);
        }
        //
        // Get the column count. Are we inserting all columns or just the specific columns?
        $columnCount = !empty($columns) ? count($columns) : count(reset($rows));
        // Build the column list
        $columnList = !empty($columns) ? '(' . implode(', ', $columns) . ')' : '';
        // Build value placeholders for single row
        $rowPlaceholder = ' (' . implode(', ', array_fill(1, $columnCount, '?')) . ')';
        // Build the whole prepared query

        $query = sprintf(
            'INSERT INTO %s%s VALUES %s',
            $table,
            $columnList,
            implode(', ', array_fill(1, count($rows), $rowPlaceholder))
        );
        // Prepare PDO statement
        try {
            $statement = $this->connection->prepare($query);
        } catch (PDOException  $e) {
            return array('errores' => ['prepare_statement'], 'message' => $e->getMessage(), 'success' => false);
        }
        //
        // // Flatten the value array (we are using ? placeholders)
        $data = array();
        foreach ($rows as $rowData) {
            $keys = array_keys($rowData);
            foreach ($columns as $columna) {
                array_push($data, $rowData[$columna]);
            }
        }
        // Did the insert go successfully?
        try {
            if ($withTransaction) {
                $this->connection->beginTransaction();
            }
            $result = $statement->execute($data);
            if (!$result) {
                if ($withTransaction) {
                    $this->connection->rollback();
                }
                return array('errores' => ['statement_execute'], 'message' => $statement->errorInfo(), 'success' => false);
            }

            $last_id = $this->connection->lastInsertId();
            if ($withTransaction) {
                $this->connection->commit();
            }

            #foreach ($this->connection->query('SELECT LAST_INSERT_ID() as last_id') as $row) {
            #    $last_id = intval($row['last_id']);
            #}
            $ids = [];
            for ($i = 0; $i < count($rows); $i++) {
                $idN = $i + $last_id;
                array_push($ids, $idN);
            }
            return ['data' => $ids, 'success' => true];
        } catch (PDOException $e) {
            if ($withTransaction) {
                $this->connection->rollback();
            }
            return array('errores' => ['rollback'], 'message' => $e->getMessage(), 'success' => false);
        }
        return ['data' => [], 'success' => false];
    }

    /**
     * @param string $table
     * @param array $new
     * @param array $conditions
     * @return bool
     */
    public function update(string $table, array $new, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): array
    {
        // Is array empty? Nothing to update! 

        if (empty($new)) {
            return ['success' => false, 'message' => 'Nothing to be updated'];
        }

        if ($operator !== 'AND' && $operator !== 'OR') {
            return ['success' => false, 'message' => 'Only AND / OR operator'];
        }

        $changeArray = [];
        $arrayValue = [];
        foreach ($new as $col => $val) {
            array_push($changeArray, $col . "=?");
            array_push($arrayValue, $val);
        }
        //
        $conditionsObjs = [];

        if (empty($conditions)) {
            array_push($conditionsObjs, "1");
        } else {
            foreach ($conditions as $col => $val) {
                array_push($conditionsObjs, $col . "=? ");
                array_push($arrayValue, $val);
            }
        }


        $query = sprintf(
            'UPDATE %s SET %s WHERE %s',
            $table,
            implode(" , ", $changeArray),
            implode(" $operator ", $conditionsObjs)
        );

        // Prepare PDO statement
        try {
            $statement = $this->connection->prepare($query);
        } catch (PDOException  $e) {
            return array('message' => $e->getMessage(), 'success' => false);
        }

        try {
            if ($withTransaction) {
                $this->connection->beginTransaction();
            }

            if ($statement->execute($arrayValue)) {
                if ($withTransaction) {
                    $this->connection->commit();
                }
                $count_updated = $statement->rowCount();
                if ($statement->rowCount()) {
                    return array('message' => 'Data updated successfully', 'success' => true, 'updated_rows' => $count_updated);
                } else {
                    return array('message' => 'No data were updated', 'success' => true, 'updated_rows' => $count_updated);
                }
            } else {
                if ($withTransaction) {
                    $this->connection->rollback();
                }
                return array('message' => 'Error in update', 'success' => false);
            }
        } catch (PDOException $e) {
            if ($withTransaction) {
                $this->connection->rollback();
            }
            return array('message' => $e->getMessage(), 'success' => false);
        }
    }

    public function delete(string $table, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): array
    {
        if ($operator !== 'AND' && $operator !== 'OR') {
            return ['success' => false, 'message' => 'Only AND / OR operator'];
        }

        $arrayValue = [];
        $conditionsObjs = [];

        if (empty($conditions)) {
            array_push($conditionsObjs, "1");
        } else {
            foreach ($conditions as $col => $val) {
                array_push($conditionsObjs, $col . "=? ");
                array_push($arrayValue, $val);
            }
        }


        $query = sprintf(
            'DELETE FROM  %s WHERE %s',
            $table,
            implode(" $operator ", $conditionsObjs)
        );

        // Prepare PDO statement
        try {
            $statement = $this->connection->prepare($query);
        } catch (PDOException  $e) {
            return array('message' => $e->getMessage(), 'success' => false);
        }

        try {
            if ($withTransaction) {
                $this->connection->beginTransaction();
            }
            if ($statement->execute($arrayValue)) {
                if ($withTransaction) {
                    $this->connection->commit();
                }
                return array('message' => 'Data deleted successfully', 'success' => true);
            } else {
                if ($withTransaction) {
                    $this->connection->rollback();
                }
                return array('message' => 'Error in update', 'success' => false);
            }
        } catch (PDOException $e) {
            if ($withTransaction) {
                $this->connection->rollback();
            }
            return array('message' => $e->getMessage(), 'success' => false);
        }
    }

    /**
     * @param $a
     * @param $b
     * @return bool
     */
    private function array_equal($a, $b)
    {
        return (is_array($a)
            && is_array($b)
            && count($a) == count($b)
            && array_diff($a, $b) === array_diff($b, $a));
    }

    private function fk(string $tabla, array $obj)
    {
        $objR = array(
            'campo' => $obj['Field'],
            'tipo' => $obj['Type']
        );
        if ($obj['Key'] != '' && $obj['Key'] != 'PRI') {
            $objR['key'] = "FK";
            $sql = "SELECT
                          TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
                        FROM
                          INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                        WHERE
                          REFERENCED_TABLE_SCHEMA = '" . $this->dbName . "' AND
                          TABLE_NAME = '" . $tabla . "' AND
                          COLUMN_NAME='" . $obj['Field'] . "'";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            $keys = $stmt->fetchAll();
            foreach ($keys as  $key) {
                if ($key['REFERENCED_TABLE_NAME'] != "") {
                    $keyRelated = "";
                    $primary = 'DESCRIBE ' . $key['REFERENCED_TABLE_NAME'];
                    $stmt = $this->connection->prepare($primary);
                    $stmt->execute();
                    $objRelated = $stmt->fetchAll();
                    foreach ($objRelated as $campo) {
                        if ($campo['Key'] == 'PRI') {
                            $keyRelated = $campo['Field'];
                            break;
                        }
                    }
                    $query = 'SELECT * FROM ' . $key['REFERENCED_TABLE_NAME'];

                    $stmt = $this->connection->prepare($query);
                    $stmt->execute();
                    $hijos = $stmt->fetchAll();

                    foreach ($hijos as $key1 => $hijo) {
                        foreach ($hijo as $attr => $value) {
                            $hijos[$key1][$attr] = utf8_encode($value);
                            if ($attr == $keyRelated) {
                                $hijos[$key1][base64_encode('id')] = utf8_encode($value);
                            }
                        }
                        json_encode($hijos[$key1][$attr]);
                        switch (json_last_error()) {
                            case JSON_ERROR_NONE:
                                echo '';
                                break;
                            case JSON_ERROR_DEPTH:
                                // var_dump($hijo);
                                echo ' - Maximum stack depth exceeded';
                                break;
                            case JSON_ERROR_STATE_MISMATCH:
                                // var_dump($hijo);
                                echo ' - Underflow or the modes mismatch';
                                break;
                            case JSON_ERROR_CTRL_CHAR:
                                // var_dump($hijo);
                                echo ' - Unexpected control character found';
                                break;
                            case JSON_ERROR_SYNTAX:
                                // var_dump($hijo);
                                echo ' - Syntax error, malformed JSON';
                                break;
                            case JSON_ERROR_UTF8:
                                // var_dump($hijo);
                                echo "<br><br>";
                                echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                                echo "<br><br>";
                                break;
                            default:
                                // var_dump($hijo);
                                echo ' - Unknown error';
                                break;
                        }
                    }
                    $objR['referencia_tabla'] = $key['REFERENCED_TABLE_NAME'];
                    $objR['referencia_columna'] = $key['REFERENCED_COLUMN_NAME'];
                    $objR['hijos'] = json_encode($hijos);
                }
            }
        }
        if ($obj['Key'] == 'PRI') {
            $objR['key'] = "PK";
        }
        if ($obj['Key'] == '') {
            $objR['key'] = "None";
        }
        return $objR;
    }
}
