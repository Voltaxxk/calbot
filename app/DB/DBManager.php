<?php

namespace App\DB;

use PDO;
use Exception;
use App\Config\Config;
use App\DB\Interfaces\DBInterface;

class DBManager
{

    /**
     * @var App\DB\Interfaces\DBInterface
     */
    private $db;

    public function __construct(string $keyDB = 'default')
    {
        $response = $this->makeDB($keyDB);

        if (!$response['success']) {
            throw new Exception($response['message']);
        }
    }

    public function beginTransaction()
    {
        if ($this->db) {
            $this->db->beginTransaction();
        }
    }

    public function commit()
    {
        if ($this->db) {
            $this->db->commit();
        }
    }
    public function rollback()
    {
        if ($this->db) {
            $this->db->rollback();
        }
    }

    public function raw(string $query, array $values = []): ? array
    {
        $retorno = null;
        if ($this->db) {
            $retorno = $this->db->raw($query, $values);
        }
        return $retorno;
    }

    /**
     * @param string $table
     * @param array $rows
     * @param array $columns
     * @return mixed
     */
    public function insert(string $table, array $rows, array $columns, bool $withTransaction = true): ? array
    {
        $retorno = null;
        if ($this->db) {
            $retorno = $this->db->insert($table, $rows, $columns, $withTransaction);
        }
        return $retorno;
    }

    public function update(string $table, array $new, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): ? array
    {
        $retorno = null;
        if ($this->db) {
            $retorno = $this->db->update($table, $new, $conditions, $withTransaction, $operator);
        }
        return $retorno;
    }

    public function delete(string $table, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): ? array
    {
        $retorno = null;
        if ($this->db) {
            $retorno = $this->db->delete($table, $conditions, $withTransaction, $operator);
        }
        return $retorno;
    }

    public function getColumns(string $table, array $conditions = array(), array $columns = array(), string $operator = 'AND'): ? array
    {
        if ($this->db) {
            return $this->db->getColumns($table, $conditions, $columns, $operator);
        }
        return null;
    }

    public function num_rows(string $query)
    {
        return $this->db->num_rows($query);
    }

    public function describeTable(string $table)
    {
        return $this->db->describeTable($table);
    }

    // Getters & Setters
    public function getHost()
    {
        return $this->db->getHost();
    }

    public function setHost(string $host)
    {
        return $this->db->setHost($host);
    }

    public function getDBName()
    {
        return $this->db->getDBName();
    }

    public function setDBName(string $dbName)
    {
        return $this->db->setDBName($dbName);
    }

    public function getUser()
    {
        return $this->db->getUser();
    }

    public function setUser(string $user)
    {
        return $this->db->setUser($user);
    }

    public function setPassword(string $password)
    {
        return $this->db->setPassword($password);
    }

    public function getEngine()
    {
        return $this->db->getEngine();
    }

    private function makeDB(string $keyDB) {

        $response = [];

        $success = false;

        try {

            $config = Config::getDBConfig($keyDB);

            $engine = $config["engine"];
            $host = $config["host"];
            $user = $config["user"];
            $pass = $config["password"];
            $dbname = $config["db"];

            DBFactory::setDriver($engine);
            $this->db = DBFactory::makeDB($host, $user, $pass, $dbname);
            
            if ($this->db) {
                $success = true;
                $message = "DB Instance created successfully";
            } else  {
                $success = false;
                $message = "DB Instance was not created";
            }
        }
        catch (Exception $exception) {
            $message = $exception->getMessage();
        }

        $response['success'] = $success;
        $response['message'] = $message;
        return $response;
    }
    
}
