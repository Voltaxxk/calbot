<?php

namespace App\DB;

use App\DB\Interfaces\DBInterface;

abstract class DB implements DBInterface
{
    protected $engine;
    protected $connection;
    protected $host;
    protected $dbName;
    protected $user;
    protected $password;

    public abstract function connect();

    public abstract function beginTransaction();
    public abstract function commit();
    public abstract function rollback();

    public abstract function raw(string $query, array $values = []): array;
    public abstract function insert(string $table, array $rows, array $columns, bool $withTransaction = true): array;
    public abstract function update(string $table, array $new, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): array;
    public abstract function delete(string $table, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): array;

    public abstract function getColumns(string $table, array $conditions = array(), array $columns = array(), string $operator = 'AND'): array;
    public abstract function describeTable(string $table);
    public abstract function num_rows(string $query);

    public function getHost()
    {
        return $this->host;
    }

    public function setHost(string $host): bool
    {
        $resultado = false;
        if (strlen($host) > 0) {
            $this->host = $host;
            $resultado = true;
        }
        return $resultado;
    }

    public function getDBName()
    {
        return $this->dbName;
    }

    public function setDBName(string $dbName): bool
    {
        $resultado = false;
        if (strlen($dbName) > 0) {
            $this->dbName = $dbName;
            $resultado = true;
        }
        return $resultado;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(string $user): bool
    {
        $resultado = false;
        if (strlen($user) > 0) {
            $this->user = $user;
            $resultado = true;
        }
        return $resultado;
    }

    public function setPassword(string $password): bool
    {
        $resultado = false;
        if (strlen($password) > 0) {
            $this->password = $password;
            $resultado = true;
        }
        return $resultado;
    }

    public function getEngine()
    {
        return $this->engine;
    }
}
