<?php

namespace App\DB;

use DB;

use App\DB\Engines\MySQLDB;
use App\DB\Interfaces\DBInterface;
use Exception;

class DBFactory
{
    protected static $driver = null;

    public static function setDriver($driver)
    {
        self::$driver = $driver;
    }

    public static function makeDB($host, $user, $pass, $dbname): ? DBInterface
    {
        $DB = null;

        switch (self::$driver) {
            case 'mysql':
                $DB = new MySQLDB();
                break;
            case 'postgre':
                $DB = new PostgreSQLDB();
                break;
            case 'sqlite':
                $DB = new SQLiteDB();
                break;
            default:
                $DB = new MySQLDB();
                break;
        }

        $DB->setHost($host);
        $DB->setDBName($dbname);
        $DB->setUser($user);
        $DB->setPassword($pass);
        $response = $DB->connect();

        if (!$response['success']) {
            throw new Exception( $response['message'] );
        }
        return $DB;
    }
}
