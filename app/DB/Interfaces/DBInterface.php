<?php

namespace App\DB\Interfaces;

interface DBInterface
{

    function connect();

    function beginTransaction();
    function commit();
    function rollback();

    function raw(string $query, array $values = []): array;
    function insert(string $table, array $rows, array $columns, bool $withTransaction = true): array;
    function update(string $table, array $new, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): array;
    function delete(string $table, array $conditions = array(), bool $withTransaction = true, string $operator = 'AND'): array;

    function getColumns(string $table, array $conditions = array(), array $columns = array(), string $operator = 'AND'): array;
    function num_rows(string $query);
    function describeTable(string $table);

    // Getters & Setters
    function getHost();
    function getDBName();
    function getUser();
    function getEngine();

    function setHost(string $host): bool;
    function setDBName(string $dbName): bool;
    function setUser(string $user): bool;
    function setPassword(string $password): bool;
}
