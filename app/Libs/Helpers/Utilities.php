<?php

namespace App\Libs\Helpers;

class Utilities
{
    public static function greet(string $greeting)
    {
        echo $greeting;
    }

    public static function calculateMean(array $values): float
    {
        $mean = 0;
        $n = count($values);
        if ($n > 0) {
            $mean = array_sum($values) / $n;
        }
        return $mean;
    }
}
