<?php

namespace App\Http;

use Illuminate\Support\Traits\Macroable;
use Illuminate\Http\ResponseTrait;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Illuminate\Http\Response as LaravelResponse;

class Response extends LaravelResponse
{
    use ResponseTrait, Macroable {
        Macroable::__call as macroCall;
    }

    /**
     * Create a new HTTP response.
     *
     * @param  mixed  $content
     * @param  int  $status
     * @param  array  $headers
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($content = '', $status = 200, array $headers = [])
    {
        $this->headers = new ResponseHeaderBag($headers);

        $this->setContent($content);
        $this->setStatusCode($status);
        $this->setProtocolVersion('1.0');
    }

    public function send()
    {
        $this->sendCORSHeaders();
        return parent::send();
    }

    private function sendCORSHeaders()
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $this->header('Access-Control-Allow-Origin', "{$_SERVER['HTTP_ORIGIN']}");
            $this->header('Access-Control-Allow-Credentials', 'true');
            $this->header('Access-Control-Max-Age', '86400');
        } else {
            $this->header('Access-Control-Allow-Credentials', 'true');
            $this->header('Access-Control-Allow-Origin', '*');
        }
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            $this->header('Access-Control-Allow-Headers', "{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        } else {
            $this->header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        }
        $this->header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, PUT, DELETE');
        return $this;
    }
}
