<?php

namespace App\Services;

class REST
{

    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    protected $_headers = array();

    protected $_method = "";
    private $_code = 200;

    public function __construct()
    {
        $this->get_headers();
        $this->get_files();
        $this->inputs();
    }

    public function get_referer()
    {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status)
    {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }


    public function json($data)
    {
        return json_encode($data, JSON_PRETTY_PRINT);
    }

    private function get_status_message()
    {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function get_files()
    {
        $this->_files = $_FILES;
    }

    private function inputs()
    {
        $this->_method = $this->get_request_method();
        switch ($this->_method) {
            case "POST":
                $result = file_get_contents("php://input");
                if ($result === FALSE) {
                    $this->_request = $this->cleanInputs($_POST);
                } else {
                    $this->_request = json_decode($result, true);
                    if ($this->_request === NULL) {
                        $this->_request = $this->cleanInputs($_POST);
                    } else {
                        $this->_request = $this->cleanInputs($this->_request);
                    }
                }
                break;
            case "GET":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            case "OPTIONS":
                $result = file_get_contents("php://input");
                if ($result === FALSE) {
                    $this->_request = $this->cleanInputs($_POST);
                } else {
                    $this->_request = json_decode($result, true);
                    if ($this->_request === NULL) {
                        $this->_request = $this->cleanInputs($_POST);
                    } else {
                        $this->_request = $this->cleanInputs($this->_request);
                    }
                }
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data)
    {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            $data = trim(stripslashes($data));
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function get_headers()
    {
        if (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $this->_headers = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        } else {
            $this->_headers = $this->get_headers_nginx();
        }
    }

    private function get_headers_nginx()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    public function get_header(string $header)
    {
        $infoHeader = null;
        if ($this->_headers != null) {
            $infoHeader = (isset($this->_headers[$header]) ? $this->_headers[$header] : null);
        }
        return $infoHeader;
    }

    public function get_authorization_header(string $authorization)
    {
        $infoHeader = $this->get_header('Authorization');
        $infoAuthorization = null;
        if ($infoHeader != null) {
            switch ($authorization) {
                case 'Bearer':
                    if (!empty($infoHeader)) {
                        if (preg_match('/Bearer\s(\S+)/', $infoHeader, $matches)) {
                            $infoAuthorization = $matches[1];
                        }
                    }
                    break;
            }
        }
        return $infoAuthorization;
    }

    private function set_headers()
    {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // you want to allow, and if so:
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        } else {
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Allow-Origin: *');
        }

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        } else {
            header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        }
        header('Access-Control-Allow-Methods: GET, OPTIONS, POST, PUT, DELETE');
        header("Content-Type: " . $this->_content_type . "; charset=utf-8");
    }
}
