<?php

namespace App\Config;

use Exception;

class Config
{
    private static $config = array(
        'DB' =>  __DIR__ . "/data/database.json",
    );

    public static function getDBConfig(string $keyDB): ? array
    {
        $json = file_get_contents(self::getValues('DB'));
        if ($json === FALSE) {
            throw new Exception('DB Config File Not Found');
        }

        $DBGlobalConfig = json_decode($json, true);
        if (!$DBGlobalConfig) {
            throw new Exception('DB Config Load Failed');
        }

        if (!isset($DBGlobalConfig[$keyDB])) {
            throw new Exception("DB {$keyDB} Key Load Failed");
        }

        $DBConfig = $DBGlobalConfig[$keyDB];

        if (!is_array($DBConfig)) {
            throw new Exception("DB {$keyDB} Invalid Config");
        }

        $checkDBConfig = self::checkDBConfig($DBConfig);

        if (!$checkDBConfig['success']) {
            throw new Exception( $checkDBConfig['message'] );
        }
        return $DBConfig;
    }

    private static function checkDBConfig(array $config)
    {
        $response = [];
        $success = false;
        $message = "";
        if (!$config) {
            $message = "Config Load Failed";
        } else {

            if (!isset($config['engine'])) {
                $message .= "DB Engine not defined\n";
            }

            if (!isset($config['host'])) {
                $message .= "DB Host not defined\n";
            }

            if (!isset($config['user'])) {
                $message .= "DB User not defined\n";
            }

            if (!isset($config['password'])) {
                $message .= "DB Password not defined\n";
            }

            if (!isset($config['db'])) {
                $message .= "DB Name not defined\n";
            }
        }

        if (strlen($message) == 0) {
            $success = true;
        }

        $response['success'] = $success;
        $response['message'] = $message;

        return $response;
    }

    private static function getValues(string $key): ?string
    {
        $config = null;
        if (array_key_exists($key, self::$config)) {
            $config = self::$config[$key];
        }
        return $config;
    }

}
 