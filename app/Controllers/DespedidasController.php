<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


date_default_timezone_set('UTC'); 
class DespedidasController extends Controller
{
    public function index()
    {
        echo "Api Topicos Works!";
    }

    /**
     * Get the specified bot.
     *
     * @param  string  $id
     * @return Response
     */
    public function show($id)
    {
        $idBot = $id;
        $db = new DBManager;
        $resultado = $db->raw('SELECT * FROM topico WHERE IdBot = ? AND Estado = "A"',[$idBot]);

        // $resultado = $db->getColumns('usuarios', ['id' => $id]);
        return response($resultado);
    }

    public function data($id){

        $idTopico = $id;
        $db = new DBManager;
    
        $response = $db->raw('SELECT * FROM contestacion WHERE IdTopico = ? AND Estado = "A"',[$idTopico]);
        $contestacion = [];
        foreach($response['data'] as $row => $item){
             
            $preData = [

                    "idContestacion" => $item['IdContestacion'],
                    "idTopico" => $item['IdTopico'],
                    "frase" => $item['Frase']

            ];

            array_push($contestacion,$preData);

        }

        $response = $db->raw('SELECT * FROM claveuno WHERE IdTopico = ? AND Estado = "A"',[$idTopico]);
        $claveUno = [];
        foreach($response['data'] as $row => $item){

            $preData = [

                "idClaveUno" => $item['IdClaveUno'],
                "idTopico" => $item['IdTopico'],
                "frase" => $item['Palabra']

        ];
            array_push($claveUno,$preData);

        }

        $response = $db->raw('SELECT * FROM clavedos WHERE IdTopico = ? AND Estado = "A"',[$idTopico]);
        $claveDos = [];
        foreach($response['data'] as $row => $item){

            $preData = [

                "idClaveDos" => $item['IdClaveDos'],
                "idTopico" => $item['IdTopico'],
                "frase" => $item['Palabra']

        ];
            array_push($claveDos,$preData);

        }

         $respuesta = [
            "success" => true,
            "message" => "Consulta Correcta",
            "contestaciones" => $contestacion,
            "claveUno" => $claveUno,
            "claveDos" => $claveDos
         ];
        return response($respuesta);
    }
 

    /**
     * Save a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {   
        try {
            $valido = $this->validate($request, [
                'idBot' => 'required',
                'frase' => 'required',
            ]);
        } catch(ValidationException $e) {
            return response(['success' => false, 'errors' => $e->errors()]);
        }
        
        $saludo = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('despedida', $saludo, array_keys($saludo), false);
        $success = $resultado['success'];
        if (!$success) {
            $db->rollback();
            $message = "No se pudo ingresar la nueva despedida";

            $respuesta =[

                "Success" => $success,
                "Error" => $message

            ];
            return response ($respuesta);
        }
        if ($success) {
            $db->commit();
            $respuesta = [

                "Success" => $success,
                "message" => "Despedida ingresada Correctamente"

            ];

            return response($respuesta);
        }
        
    }

    /**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $datos = array_keys($request->all());

        if(in_array('estado',$datos)){

            try {
                $valido = $this->validate($request, [
                    'idDespedida' => 'required',
                    'estado' => 'required',
                ]);
            } catch(ValidationException $e) {
                return response(['success' => false, 'errors' => $e->errors()]);
            }
            $hoy = date('Y-m-d H:i:s');
            $cambio = $request->all();
            $cambio['FechaModificacion'] = $hoy;
            $db = new DBManager;
            $resultado = $db->update('despedida', $cambio, [ 'idDespedida' => $cambio['idDespedida'] ]);
            return response($resultado);

        }else {

            try {
                $valido = $this->validate($request, [
                    'idDespedida' => 'required',
                    'frase' => 'required',
                ]);
            } catch(ValidationException $e) {
                return response(['success' => false, 'errors' => $e->errors()]);
            }
            $hoy = date('Y-m-d H:i:s');
            $cambio = $request->all();
            $cambio['FechaModificacion'] = $hoy;
            $db = new DBManager;
            $resultado = $db->update('despedida', $cambio, [ 'idDespedida' => $cambio['idDespedida'] ]);
            return response($resultado);

        }

    }

    public function testValidator(Request $request) 
    {
        try {
            $valido = $this->validate($request, [
                'body' => 'required',
                'email' => 'email',
                'title' => 'required'
            ]);
        } catch(ValidationException $e) {
            return response(['success' => false, 'errors' => $e->errors()]);
        }

        return response($request->all());
    }
}
