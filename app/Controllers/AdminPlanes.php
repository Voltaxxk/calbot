<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

use function PHPSTORM_META\type;

date_default_timezone_set('America/Guayaquil');

class AdminPlanes extends Controller
{

    public function index(){

        echo "Api WORKs!!";

    }
    
    /**Servicios **/
    public function servicios(){

        $db = new DBManager();
        $response = $db->raw("SELECT DISTINCT(Servicio) FROM Plan");
        $data = $response['data'];

        $respuesta = [
            "success" => true,
            "message" => "Obtencion Servicios Exitosa",
            "data" => $data
        ];    

        return response($respuesta);

    }

    public function nuevoServicio(Request $request){

        try{

            $valido = $this->validate($request, [

                'Servicio' => 'required',
                'VARIANTE_PRODUCTO' => 'required',
                'PLAN_A_PRESENTAR_EN_ROBOT' => 'required',

            ]);

        }catch(ValidationException $e) {

            return response(['success' => false, 'errors' => $e->errors()]);

        }

        $postParam = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('Plan', $postParam, array_keys($postParam), false);
        $success = $resultado['success'];
        if (!$success) {
            $db->rollback();
            $message = "No se pudo ingresar nuevo servicio";

            $respuesta =[

                "Success" => $success,
                "Error" => $message

            ];
            return response ($respuesta);
        }
        if ($success) {
            $db->commit();
            $respuesta = [

                "Success" => $success,
                "message" => "Nuevo servicio Ingresado Correctamente"

            ];

            return response($respuesta);
        }
    }

    public function editServicio(Request $request){

        $datos = $request->all();   
        $respuesta = [];

        if (in_array('Estado', array_keys($datos))){

           try {

                $valido = $this->validate($request, [
                    'IdPlan' => 'required',
                ]);

            } catch (ValidationException $e){

                return response(['success' => false, 'errors' => $e->errors()]); 

            }

            $db = new DBManager;
            $db->beginTransaction();
            $resultado = $db->update(
                'Plan', 
                ["Estado" => $datos['Estado']],
                ['IdPlan' => $datos['IdPlan']],
                false
            );

            $success = $resultado['success'];
            if ($success) {
                $db->commit();
                $respuesta = [

                    "success" => $success,
                    "message" => "Servicio eliminado exitosamente"
    
                ];
            } else {
                $db->rollback();
                $respuesta = [

                    "success" => $success,
                    "message" => "Error al eliminar el servicio"
    
                ];

            }
            return response($datos);
        } else {

            try {
                $valido = $this->validate($request, [
                    'IdPlan' => 'required'
                ]);

            } catch(ValidationException $e) {

                return response(['success' => false, 'errors' => $e->errors()]); 

            }

            $db = new DBManager;
            $db->beginTransaction();
            $resultado = $db->update(
                'Plan', 
                [
                    "Servicio" => $datos['Servicio'], 
                    "VARIANTE_PRODUCTO" => $datos['VARIANTE_PRODUCTO'],
                    "PLAN_A_PRESENTAR_EN_ROBOT" => $datos['PLAN_A_PRESENTAR_EN_ROBOT'],
                    "DESCRIPCION" => $datos['DESCRIPCION'],
                    "PRECIO" => $datos['PRECIO'],
                ],
                [
                    "IdPlan" => $datos['IdPlan']
                ],
                false       
            );
            $success = $resultado['success'];
            if($success){
                $db->commit();
                $respuesta = [

                    "success" => $success,
                    "message" => "Servicio modificado exitosamente"
    
                ];
            }else{
                $db->rollback();
                $respuesta = [

                    "success" => $success,
                    "message" => "Error al modificar el servicio"
    
                ];

            }
        }

        return response($respuesta);

    }

    public function planesServicios(Request $request){

        $getParam = $request->all();
        $nombreServicio = $getParam['nombre'];
        $db = new DBManager();
        $response = $db->raw("SELECT IdPlan, VARIANTE_PRODUCTO as tipoServicio, PLAN_A_PRESENTAR_EN_ROBOT as tipoPlan, DESCRIPCION as descripcion, PRECIO 
                               FROM `Plan` 
                               WHERE Servicio LIKE '$nombreServicio' AND 
                                     Estado = 'A'");

        $data = $response['data'];

        $respuesta = [
            "success" => true,
            "message" => "Obtencion Planes x Servicios Exitosa",
            "data" => $data
        ];    

        return response($respuesta);
    }

    /**Fin de servicio **/

    public function ciudades(){

        $db = new DBManager();
        $response = $db->raw("SELECT IdCiudadAgencia as idAgencia, NombreCiudadAgencia as nombreAgencia 
                              FROM `CiudadAgencia` 
                              WHERE Estado = 'A'");
        $data = $response['data'];

        $respuesta = [
            "success" => true,
            "message" => "Obtencion Ciudades Exitosa",
            "data" => $data
        ];    
        
        return response($respuesta);
    }

    public function agenciasCiudades(Request $request){
        $getParam = $request->all();
        $idAgencia = $getParam['id'];
        $db = new DBManager();
        $response = $db->raw("SELECT Agencia.IdAgencia as idAgencia,Agencia.IdCiudadAgencia as IdAgenciaCiudad, CiudadAgencia.NombreCiudadAgencia as nombreCiudad, Agencia.NombreAgencia as nombreAgencia  
                              FROM `Agencia` 
                              INNER JOIN CiudadAgencia ON Agencia.IdCiudadAgencia = CiudadAgencia.IdCiudadAgencia 
                              WHERE Agencia.IdCiudadAgencia = $idAgencia AND 
                                    Agencia.Estado = 'A' 
                              ORDER by IdAgencia ASC");

        $data = $response['data'];
        $respuesta = [
            "success" => true,
            "message" => "Obtencion Agencias x Ciudades Exitosa",
            "data" => $data
        ];    
        return response($respuesta);
    }

    public function nuevaAgencia(Request $request){

        try{

            $valido = $this->validate($request, [

                'IdCiudadAgencia' => 'required',
                'NombreAgencia' => 'required'

            ]);

        }catch(ValidationException $e) {

            return response(['success' => false, 'errors' => $e->errors()]);

        }

        $postParam = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('Agencia', $postParam, array_keys($postParam), false);
        $success = $resultado['success'];
        if (!$success) {
            $db->rollback();
            $message = "No se pudo ingresar nueva Agencia";

            $respuesta =[

                "Success" => $success,
                "Error" => $message

            ];
            return response ($respuesta);
        }
        if ($success) {
            $db->commit();
            $respuesta = [

                "Success" => $success,
                "message" => "Nuevo servicio Ingresado Correctamente"

            ];

            return response($respuesta);
        }
    }

    public function editAgencia(Request $request){

        $datos = $request->all();   
        $respuesta = [];

        if(in_array('Estado', array_keys($datos))){

           try{

                $valido = $this->validate($request, [
                    'IdAgencia' => 'required',
                ]);

            }catch(ValidationException $e){

                return response(['success' => false, 'errors' => $e->errors()]); 

            }

            $db = new DBManager;
            $db->beginTransaction();
            $resultado = $db->update(
                'Agencia', 
                ["Estado" => $datos['Estado']],
                ['IdAgencia' => $datos['IdAgencia']],
                false
            );
            $success = $resultado['success'];
            if($success){
                $db->commit();
                $respuesta = [

                    "success" => $success,
                    "message" => "Servicio eliminado exitosamente"
    
                ];
            }else{
                $db->rollback();
                $respuesta = [

                    "success" => $success,
                    "message" => "Error al eliminar el servicio"
    
                ];

            }
            return response($respuesta);
        }else {

            try{
                $valido = $this->validate($request, [
                    'IdAgencia' => 'required',
                ]);

            }catch(ValidationException $e){

                return response(['success' => false, 'errors' => $e->errors()]); 

            }

            $db = new DBManager;
            $db->beginTransaction();
            $resultado = $db->update(
                'Agencia', 
                [
                    "NombreAgencia" => $datos['NombreAgencia'], 
                ],
                [
                    "IdAgencia" => $datos['IdAgencia']
                ],
                false       
            );
            $success = $resultado['success'];
            if($success){
                $db->commit();
                $respuesta = [

                    "success" => $success,
                    "message" => "Servicio modificado exitosamente"
    
                ];
            }else{
                $db->rollback();
                $respuesta = [

                    "success" => $success,
                    "message" => "Error al modificar el servicio"
    
                ];

            }
        }

        return response($respuesta);

    }


    /***  Recursos  ***/
    public function recursos(){

        $db = new DBManager();
        $response = $db->raw("SELECT id, nombre
                              FROM `TipoRecurso` 
                              WHERE estado = 'A'");
        $data = $response['data'];

        $respuesta = [
            "success" => true,
            "message" => "Obtencion Tipos de Recursos Exitosos",
            "data" => $data
        ];    
        
        return response($respuesta);
    }
}