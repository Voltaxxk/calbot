<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use function PHPSTORM_META\type;

date_default_timezone_set('America/Guayaquil');

class BotReportsTele extends Controller
{

    public function index(){

        echo "Api WORKs!!";

    }
    
    /*********************************** Real time ************************************/

    /****************************** Total Personas Usando Bot *******************************/
    public function  totalUse(){

        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';


        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as total FROM `RegistroUsuario` where (Asesor <> 1) AND (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");
        $data = $response['data'];
        $result = [];
    
        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['total'];

        }
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];   

        return response($respuesta);
    }

    public function  totalUseFecha(Request $request){

        $fechas = $request->all();
        $desde = $fechas['desde'];

        if (isset($fechas['hasta'])) {
            $hasta = $fechas['hasta'];
        } else {
            $hasta = date('Y-m-d');
        }

        $realtimeFinish = $hasta . ' 23:59:59';
        $realtimeStart = $desde . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as total FROM `RegistroUsuario` where (Asesor <> 1) AND (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");
        $data = $response['data'];
        $result = [];
    
        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['total'];

        }
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];   

        return response($respuesta);
    }

    /*******************************  Fin Total Personas Usando Bot *******************************/

    /****************************** Total Personas pasaron Asesores *******************************/
    public function totalAsesores() {

        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        
        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalAsesor
                              FROM `RegistroUsuario` 
                              WHERE (asesor = 1)   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalAsesor'];

        }

        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];  

        return response($respuesta);
    }

    public function totalAsesoresFecha(Request $request) {

        $fechas = $request->all();
        $desde = $fechas['desde'];

        if (isset($fechas['hasta'])) {
            $hasta = $fechas['hasta'];
        } else {
            $hasta = date('Y-m-d');
        }

        $realtimeFinish = $hasta . ' 23:59:59';
        $realtimeStart = $desde . ' 00:00:00';

        
        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalAsesor
                              FROM `RegistroUsuario` 
                              WHERE (asesor = 1)   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalAsesor'];

        }

        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];  

        return response($respuesta);
    }
    
    /****************************** Total Personas Pasaron Asesores *******************************/

    /****************************** Total Perssonas que usaron topico Ventas *******************************/
    public function topicoVenta() {
        
        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                              FROM `RegistroTopico` 
                              WHERE (Anterior ='Ventas')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];  

        return response($respuesta);
    }

    public function topicoVentasFecha(Request $request){

        $fechas = $request->all();
        $desde = $fechas['desde'];

        if (isset($fechas['hasta'])) {
            $hasta = $fechas['hasta'];
        } else {
            $hasta = date('Y-m-d');
        }
        $realtimeFinish = $hasta . ' 23:59:59';
        $realtimeStart = $desde . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                              FROM `RegistroTopico` 
                              WHERE (Anterior ='Ventas')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];  

        return response($respuesta);

    }

    /****************************** Final Personas que usaron topico Ventas *******************************/

    /****************************** Total Perssonas que usaron topico Tv *******************************/
    public function topicoTv() {

        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                              FROM `RegistroTopico` 
                              WHERE (Anterior = 'Television')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];    
        
        return response($respuesta);
    }

    public function topicoTvFecha(Request $request) {

        $fechas = $request->all();
        $desde = $fechas['desde'];

        if (isset($fechas['hasta'])) {
            $hasta = $fechas['hasta'];
        } else {
            $hasta = date('Y-m-d');
        }
        $realtimeFinish = $hasta . ' 23:59:59';
        $realtimeStart = $desde . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                              FROM `RegistroTopico` 
                              WHERE (Actual = 'Television')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];    
        
        return response($respuesta);
    }

    /****************************** Final Personas que usaron topico Ventas *******************************/

    /****************************** Total Perssonas que usaron topico Internet *******************************/
    public function topicoInternet() {


        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                              FROM `RegistroTopico` 
                              WHERE (Anterior = 'Internet')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];    
        
        return response($respuesta);
    }

    public function topicoInternetFecha(Request $request) {

        $fechas = $request->all();
        $desde = $fechas['desde'];

        if (isset($fechas['hasta'])) {
            $hasta = $fechas['hasta'];
        } else {
            $hasta = date('Y-m-d');
        }
        $realtimeFinish = $hasta . ' 23:59:59';
        $realtimeStart = $desde . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                              FROM `RegistroTopico` 
                              WHERE (Actual = 'Internet')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa Internet",
            "data" => $result
        ];    
        
        return response($respuesta);
    }

    /****************************** Final Personas que usaron topico Ventas *******************************/

    /****************************** Total Perssonas que usaron topico Internet *******************************/
    public function topicoTvInternet() {


        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                                FROM `RegistroTopico` 
                                WHERE (Anterior = 'Duo Pack')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $result
        ];    
        
        return response($respuesta);
    }


    public function topicoTvInternetFecha(Request $request) {

        
        $fechas = $request->all();
        $desde = $fechas['desde'];

        if (isset($fechas['hasta'])) {
            $hasta = $fechas['hasta'];
        } else {
            $hasta = date('Y-m-d');
        }
        $realtimeFinish = $hasta . ' 23:59:59';
        $realtimeStart = $desde . ' 00:00:00';

        $realtimeFinish = date('Y-m-d') . ' 23:59:59';
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw("SELECT COUNT(*) as totalTopVentas
                                FROM `RegistroTopico` 
                                WHERE (Actual = 'Duo Pack')   AND  
                                    (Fecha_Creacion BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = $response['data'];
        $result = [];

        if(empty($data)){

            $result['total'] = 0;

        }else {

            $result['total'] = $data[0]['totalTopVentas'];

        }
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa Duo Pack",
            "data" => $result
        ];    
        
        return response($respuesta);
    }
    
        /****************************** Final Personas que usaron topico Ventas *******************************/
}