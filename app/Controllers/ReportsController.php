<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;

use function PHPSTORM_META\type;

// $urlBaseOmar = 'http://localhost/omar-backend/services/ReportsBotMarce/';

class ReportsController extends Controller
{   
    public function atencionesBotAsesores(Request $request)
    {
        $urlBaseOmar = 'http://localhost/omar-backend/services/ReportsBotMarce/';

        $request  = $request->all();
        if(isset($request['year'], $request['month'])){

            $cantDias = 0;
            $cantTotalWb = 0;
            $promdPorcWb = 0;
            $cantTotalWS = 0;
            $promdPorcWs = 0;
            $year = $request['year'];
            $month = $request['month'];
            $orden = ['Fecha', 'Web','PorcentajeWeb','WhatsApp','PorcentajeWha','TotalWbWs','TotalInter','ResueltoBot','PorcentajeBot','TotalAsesor','PorcentajeAse'];
            $db = new DBManager();

            $client = new Client([
                'base_uri' => $urlBaseOmar,
                'timeout'  => 2.0,
            ]);

            
            $responseI = $db->raw("SELECT DATE(FechaInicioConversacionGlobal) as fecha, bot.Clase as clase, COUNT(DISTINCT(Identificacion)) as cantidad 
            FROM ConversacionGlobal 
            INNER JOIN Bot ON ConversacionGlobal.IdBot = Bot.IdBot  
            WHERE (MONTH(FechaInicioConversacionGlobal) = $month) AND (YEAR(FechaInicioConversacionGlobal) = $year) 
            GROUP BY DATE(FechaInicioConversacionGlobal), ConversacionGlobal.IdBot ORDER BY  FechaInicioConversacionGlobal ASC");
            
            $responseT = $db->raw("SELECT DATE(FechaInicioConversacionGlobal) as fecha, COUNT(Identificacion) as cantidad 
            FROM ConversacionGlobal 
            WHERE (MONTH(FechaInicioConversacionGlobal) = $month) AND (YEAR(FechaInicioConversacionGlobal) = $year) 
            GROUP BY DATE(FechaInicioConversacionGlobal) ORDER BY  FechaInicioConversacionGlobal ASC");
        
            $responseA = $client->request('GET', 'totalGroupsByDays',[

                "query" => [
                    "year" => $year,
                    "month" => $month
                ]

            ]);

            $responseA = json_decode($responseA->getBody()->getContents());
            $responseGrupos = $client->request('GET', 'getAllTopics');
            $responseGrupos = json_decode($responseGrupos->getBody()->getContents());

            if($responseI['success']){
                
                $dateSup = "";
                $preData = [];
                foreach($responseI['data'] as $key => $item){

                    $nombre = $item['clase'];

                    if($dateSup != $item['fecha']){

                        $dateSup = $item['fecha'];
                        $preData[$item['fecha']] = [];
                        $preData[$item['fecha']] = ['Fecha' => $item['fecha']];
                        $preData[$item['fecha']] += [$nombre => $item['cantidad']];

                        $cantDias++;

                    }else{

                        $preData[$item['fecha']] += [$nombre => $item['cantidad']];
                        
                    }
                }

                foreach($preData as $key => $item){

                    $acu = 0;
                    $cantWb = 0;
                    $cantWs = 0;

                    if(!array_key_exists('Web', $item)){

                        $preData[$key] += ['Web' => "0"];
                        $cantWs = $item['WhatsApp'];
                        
                    }else if(!array_key_exists('WhatsApp', $item)){

                        $preData[$key] += ['WhatsApp' => "0"];
                        $cantWb = $item['Web'];
                    }else{

                        $cantWs = $item['WhatsApp'];
                        $cantWb = $item['Web'];
                    }

                    $acu = $cantWb + $cantWs;
                
                    $preData[$key] += ["PorcentajeWeb" => round(number_format(($cantWb / $acu)*100), 2) ."%"];
                    $preData[$key] += ["PorcentajeWha" => round(number_format(($cantWs / $acu)*100),2) ."%"];
                    $preData[$key] += ['TotalWbWs' => $acu];

                    $cantTotalWb = $cantTotalWb + $cantWb;
                    $promdPorcWb = $promdPorcWb + $cantWb;

                    $cantTotalWS = $cantTotalWS + $cantWs;
                    $promdPorcWs = $promdPorcWs + $cantWs;
                }
                
            }else{

                echo "consulta fallida";

            }


            if($responseT['success']){

                foreach($responseT['data'] as $key => $item){

                $preData[$item['fecha']] += ["TotalInter" => $item['cantidad']];

                }

            }else{

                echo "Hubo un error en la consulta";

            }
            
            if($responseA->success){

                foreach($responseA->data as $key => $item){

                    if(!array_key_exists($item->fecha, $preData)){

                        $preData[$item->fecha] = [];
                        $preData[$item->fecha] += ['Fecha' => $item->fecha];
                        $preData[$item->fecha] += ['Web' => 0];
                        $preData[$item->fecha] += ['WhatsApp' => 0];
                        $preData[$item->fecha] += ['PorcentajeWeb' => '0%'];
                        $preData[$item->fecha] += ['PorcentajeWha' => '0%'];
                        $preData[$item->fecha] += ['TotalWbWs' => 0];
                        $preData[$item->fecha] += ['TotalInter' => 0];
                        $preData[$item->fecha] += [$item->grupo => $item->cantidad];

                    }else {

                
                        $preData[$item->fecha] += [$item->grupo =>  $item->cantidad];                
                    }

                }

            }else{

                echo "Ocurrio un error en la consulta";

            }    

            ksort($preData);

            foreach($responseGrupos->data as $key => $item){

                array_push($orden, $item->nombre);

            }

            foreach($preData as $preKey => $preItem){
                $acu = 0;

                foreach($responseGrupos->data as $key => $item){

                    if(!array_key_exists($item->nombre, $preItem)){

                        $preData[$preKey] += [$item->nombre => 0];
                        // $acu = $acu + $preItem[$item->nombre];
        
                    }else {

                        // $acu = $acu + $preItem[$item->nombre];

                    }

                $acu = $acu + $preData[$preKey][$item->nombre];

                }

                $preData[$preKey] += ["TotalAsesor" => $acu];

                if(intval($preData[$preKey]['TotalInter']) < $acu ){

                    $preData[$preKey] += ["ResueltoBot" => 0];
                    $preData[$preKey] += ["PorcentajeBot" => "0%"];

                }else{

                    $preData[$preKey] += ["ResueltoBot" => ($preData[$preKey]['TotalInter'] - $acu)];

                    if($preData[$preKey]['TotalInter'] > 0){
                        $porcentajeBot = (intval($preData[$preKey]['ResueltoBot']) / $preData[$preKey]['TotalInter'])*100;
                        $porcentajeBot =  round(number_format($porcentajeBot, 2));
                        $preData[$preKey] += ["PorcentajeBot" => $porcentajeBot ."%"];
                        
                    }else{
                        
                        
                        $preData[$preKey] += ["PorcentajeBot" => "0%"];

                    }


                }

                if($preData[$preKey]['TotalInter'] == 0){

                    $preData[$preKey] += ['PorcentajeAse' => "100%"];

                }else{
                    
                    $porcentaje = ($preData[$preKey]['TotalAsesor'] / $preData[$preKey]['TotalInter'])*100;

                    if($porcentaje > 0){

                        $porcentaje = round(number_format($porcentaje, 2));
                        
                    }


                    $preData[$preKey] += ['PorcentajeAse' => $porcentaje ."%"];

                }

            }
            
            $orden = array_flip($orden);
            $wordOutlier = count($orden);
            foreach($preData as $key => $item){

                uksort($preData[$key], function($a, $b) use($orden, $wordOutlier){
                    return !is_int($a) <=> !is_int($b) ?: 
                    (($orden[$a] ?? $wordOutlier) <=> ($orden[$b] ?? $wordOutlier));
                
                });
            
            }

            $response =  [
                
                'success' => true,
                'message' => "Consulta realizada con exito",
                'data' => $preData
                
            ];    
            
        }else{

            $response =  [
                
                'success' => true,
                'message' => "Falta datos como fecha de consulta mes y año",
                'data' => ""
                
            ];   
        }   

        return response($response);
    }


    public function annualReport(Request $request){

        $request = $request->all();

        if(!empty($request['year'])){

            $year = $request['year'];
            $db = new DBManager();
            $response = $db->raw("SELECT Monthname(FechaInicioConversacionGlobal) as mes,
                                            COUNT(if(IdBot = 1,1,null)) as webChat,
                                            CONCAT(round(((COUNT(if(IdBot = 1,1,null)) / COUNT(IdBot))*100),2),'%') as avgWebChat, 
                                            COUNT(if(IdBot = 2,1,null)) as whatsApp, 
                                            CONCAT(round(((COUNT(if(IdBot = 2,1,null)) / COUNT(IdBot))*100),2),'%') as avgWhatsApp, 
                                            COUNT(IdBot) as total 
                                  FROM `ConversacionGlobal` 
                                  WHERE YEAR(FechaInicioConversacionGlobal) = $year 
                                  GROUP BY mes
                                  ORDER BY MONTH(FechaInicioConversacionGlobal) ASC");

            if($response['success'] == true){

                $success = true;
                $message = "Consulta realizada con exito";
                $data = $response['data'];
                
            }else{

                $success = false;
                $message = "Hubo un error ";
                $data = "";
            }

            $response = [

                'success' => $success,
                'message' => $message,
                'data' => $data
            ];

        }else {

            $response =  [
                
                'success' => true,
                'message' => "Ha faltado el año de consulta",
                'data' => ""
                
            ];       

        }

        return response($response);            

    }

    public function accumulatedAnnual(Request $request){

        $request = $request->all();

        if(!empty($request['year'])){

            $year = $request['year'];
            $db = new DBManager();
            $result = $db->raw("SELECT MONTHNAME(FechaInicioConversacionGlobal) as mes,   
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=1,1,null)) as first,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=2,1,null)) as second,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=3,1,null)) as third,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=4,1,null)) as fourth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=5,1,null)) as fifth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=6,1,null)) as sixth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=7,1,null)) as seventh,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=8,1,null)) as eighth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=9,1,null)) as ninth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=10,1,null)) as tenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=11,1,null)) as eleventh,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=12,1,null)) as twelfth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=13,1,null)) as thirteenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=14,1,null)) as fourteenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=15,1,null)) as fifteenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=16,1,null)) as sixteenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=17,1,null)) as seventeenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=18,1,null)) as eighteenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=19,1,null)) as nineteenth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=20,1,null)) as twentieth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=21,1,null)) as twentyFirst,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=22,1,null)) as twentySecond,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=23,1,null)) as twentyThird,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=24,1,null)) as twentyFourth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=25,1,null)) as twentyFifth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=26,1,null)) as twentySixth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=27,1,null)) as twentySeventh,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=28,1,null)) as twentyEighth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=29,1,null)) as twentyNinth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=30,1,null)) as thirtieth,
                                        COUNT(if(DAY(FechaInicioConversacionGlobal)=31,1,null)) as thirtyFirst,
                                        COUNT(FechaInicioConversacionGlobal) as total
                                FROM `ConversacionGlobal` 
                                WHERE YEAR(FechaInicioConversacionGlobal) = $year 
                                GROUP BY mes 
                                ORDER BY MONTH(FechaInicioConversacionGlobal) ASC");
            $success = $result['success'];
            
            if($success){

                $message = "Consulta acumulado anual de cantidad intereaccion por dias agrupado mensualmente con exito";
                $data = $result['data'];

            }else{

                $message = "Ocurrio un error en la consulta";
                $data = "";

            }
            
        }else {


            $success = false;
            $message = "No existe el campo year en el request";
            $data = "";
    
        }

        $response = [

            'success' => $success,
            'message' => $message,
            'data' => $data

        ];

        return response($response);
        
    }


    public function reportMonthByHours (Request $request){

        $urlBaseOmar = 'http://localhost/omar-backend/services/ReportsBotMarce/';

        $request = $request->all();

        if(!empty($request['year'] && $request['month'])){

            $year = $request['year'];
            $month = $request['month'];
            $startTime = "00:00:00";
            $endTime = "23:59:59";
            $startDate = $year."-".$month;
            $endDate = $year."-".$month;
            $preData = [];
            $orden = ['Web','WhatsApp', 'TotalBot', 'PeticionAsesor', 'IngresosEfectivos', 'AtencionAsesores', 'Abandono', 'TiempoEspera', 'TiempoAsesor', 'Efectividad'];
            $fillGroups = [                            
            'grupo' => '',
            'twelfthAm' => 0,
            'oneAm' => 0,
            'twoAm' => 0,
            'threeAm' => 0,
            'fourAm' => 0,
            'fiveAm' => 0,
            'sixAm' => 0,
            'sevenAm' => 0,
            'eightAm' => 0,
            'nineAm' => 0,
            'tenAm' => 0,
            'elevenAm' => 0,
            'twelfthPm' => 0,
            'onePm' => 0,
            'twoPm' => 0,
            'threePm' => 0,
            'fourPm' => 0,
            'fivePm' => 0,
            'sixPm' => 0,
            'sevenPm' => 0,
            'eightPm' => 0,
            'ninePm' => 0,
            'tenPm' => 0,
            'elevenPm' => 0,
            'total' => 0,
            'percent' => '0%'];
            $queryHours = " COUNT(if(HOUR(FechaInicioConversacionGlobal)=0,1,null)) as twelfthAm,
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=1,1,null)) as oneAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=2,1,null)) as twoAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=3,1,null)) as threeAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=4,1,null)) as fourAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=5,1,null)) as fiveAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=6,1,null)) as sixAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=7,1,null)) as sevenAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=8,1,null)) as eightAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=9,1,null)) as nineAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=10,1,null)) as tenAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=11,1,null)) as elevenAm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=12,1,null)) as twelfthPm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=13,1,null)) as onePm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=14,1,null)) as twoPm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=15,1,null)) as threePm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=16,1,null)) as fourPm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=17,1,null)) as fivePm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=18,1,null)) as sixPm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=19,1,null)) as sevenPm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=20,1,null)) as eightPm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=21,1,null)) as ninePm, 
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=22,1,null)) as tenPm,
                            COUNT(if(HOUR(FechaInicioConversacionGlobal)=23,1,null)) as elevenPm,
                            COUNT(fechaInicioConversacionGlobal) as total";

            $queryHousrTopico = "   COUNT(if(HOUR(FechaInicioConversacionTopico)=0,1,null)) as twelfthAm,
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=1,1,null)) as oneAm,
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=2,1,null)) as twoAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=3,1,null)) as threeAm,  
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=4,1,null)) as fourAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=5,1,null)) as fiveAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=6,1,null)) as sixAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=7,1,null)) as sevenAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=8,1,null)) as eightAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=9,1,null)) as nineAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=10,1,null)) as tenAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=11,1,null)) as elevenAm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=12,1,null)) as twelfthPm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=13,1,null)) as onePm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=14,1,null)) as twoPm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=15,1,null)) as threePm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=16,1,null)) as fourPm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=17,1,null)) as fivePm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=18,1,null)) as sixPm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=19,1,null)) as sevenPm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=20,1,null)) as eightPm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=21,1,null)) as ninePm, 
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=22,1,null)) as tenPm,
                                    COUNT(if(HOUR(FechaInicioConversacionTopico)=23,1,null)) as elevenPm,
                                    COUNT(FechaInicioConversacionTopico) as total" ;
               

            $numberOfDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $client = new Client([
                'base_uri' => $urlBaseOmar,
                'timeout'  => 2.0,
            ]);
            $db = new DBManager();

            for ($dayIndex = 1 ; $dayIndex <= $numberOfDays; $dayIndex++) {

                
                if($dayIndex <10){
                    $startDateTime= $startDate."-0".$dayIndex." ".$startTime;
                    $endDateTime = $startDate."-0".$dayIndex." ".$endTime; 
                }else{

                    $startDateTime = $startDate."-".$dayIndex." ".$startTime;
                    $endDateTime = $startDate."-".$dayIndex." ".$endTime;

                }

             
                $queryGroups = "SELECT bot.Clase as grupo,".$queryHours.", 
                                CONCAT(ROUND((IFNULL(COUNT(fechaInicioConversacionGlobal)/(SELECT COUNT(FechaInicioConversacionGlobal) FROM `conversacionglobal` WHERE (FechaInicioConversacionGlobal BETWEEN '$startDateTime' AND '$endDateTime')),0)*100),0),'%') as percent 
                                FROM `conversacionglobal` 
                                INNER JOIN bot On conversacionglobal.IdBot = bot.IdBot 
                                WHERE (FechaInicioConversacionGlobal BETWEEN '$startDateTime' AND '$endDateTime') 
                                GROUP BY grupo;";

                $queryTotal = "SELECT @total as grupo,".$queryHours.",
                               CONCAT(ROUND((IFNULL(COUNT(FechaInicioConversacionGlobal)/COUNT(FechaInicioConversacionGlobal),0)*100),0),'%') as percent 
                               FROM `conversacionglobal` 
                               WHERE (FechaInicioConversacionGlobal BETWEEN '$startDateTime' AND '$endDateTime')";

                $queryPeticionAsesor = "SELECT 'PeticionAsesor' as grupo,".$queryHousrTopico.", 
                                        CONCAT(ROUND((IFNULL(COUNT(FechaInicioConversacionTopico)/(SELECT COUNT(FechaInicioConversacionGlobal) FROM `conversacionglobal` WHERE (FechaInicioConversacionGlobal BETWEEN '2021-10-01 00:00:00' AND '2021-10-01 23:59:59')),0)*100),0),'%') as percent
                                        FROM `conversaciontopico` 
                                        WHERE Topico like 'Comunicarse con asesor' AND (FechaInicioConversacionTopico BETWEEN '$startDateTime' AND '$endDateTime')";

                $resulset = $db->raw($queryGroups);
                $resulsetTotal = $db->raw($queryTotal);
                $resultadoPeticiones = $db->raw($queryPeticionAsesor);
                
                $success = $resulset['success'];
                $successTotal = $resulsetTotal['success'];
                $successPeticion = $resultadoPeticiones['success'];

                if($success){
                    
                    $responseOmar = $client->request('GET', 'reportMonthByHours',[

                        "query" => [
                            "startDateTime" => $startDateTime,
                            "endDateTime" => $endDateTime
                        ]
        
                    ]);

                    $responseOmar = json_decode($responseOmar->getBody()->getContents());

                    $indexDate = substr($startDateTime, 0,10);
                    
                    if(empty($resulset['data'])){

            
                        $preData[$indexDate] = [];

                        $fillGroups['grupo'] = 'Web';
                        $preData[$indexDate] += ['Web' => $fillGroups];

                        $fillGroups['grupo'] = 'WhatsApp';
                        $preData[$indexDate] += ['WhatsApp' => $fillGroups];

                        $fillGroups['grupo'] = 'TotalBot';
                        $preData[$indexDate] += ['TotalBot' => $fillGroups];

                        $fillGroups['grupo'] = 'PeticionAsesor';
                        $preData[$indexDate] += ['PeticionAsesor' => $fillGroups];
                       
                        $fillGroups['grupo'] = '';
                    
                    }else{

                        $preData[$indexDate] = [];                               

                        foreach($resulset['data'] as $key => $item){

                            $preData[$indexDate] += [$item['grupo'] => $item];
                        }

                        if(!array_key_exists('Web', $preData[$indexDate])){

                            $fillGroups['grupo'] = 'Web';
                            $preData[$indexDate] += ['Web' => $fillGroups];
                            $fillGroups['grupo'] = '';
                            
                        }else if(!array_key_exists('WhatsApp', $preData[$indexDate])){

                            $fillGroups['grupo'] = 'WhatsApp';
                            $preData[$indexDate] += ['WhatsApp' => $fillGroups];
                            $fillGroups['grupo'] = '';

                        }

                        $resulsetTotal['data'][0]['grupo'] = 'TotalBot';
                        $preData[$indexDate] += ['TotalBot' => $resulsetTotal['data'][0]];

                        $preData[$indexDate] += ['PeticionAsesor' => $resultadoPeticiones['data'][0]];

                        
                    }

                    foreach($responseOmar->data as $key=>$item){

                        $preData[$indexDate] += [$key => $item];

                    }

                }

            }

         $message ="Consulta exitosa";   

        }else {

            $success = false;
            $message = "Faltan datos year and month";
            $data = "";
        }

        $orden = array_flip($orden);
        $wordOutlier = count($orden);
        foreach($preData as $key => $item){

            uksort($preData[$key], function($a, $b) use($orden, $wordOutlier){
                return !is_int($a) <=> !is_int($b) ?: 
                (($orden[$a] ?? $wordOutlier) <=> ($orden[$b] ?? $wordOutlier));
            
            });
        
        }

        $response = [

            'success' => $success,
            'message' => $message,
            'data' => $preData

        ];

        return response($response);

    }
}
