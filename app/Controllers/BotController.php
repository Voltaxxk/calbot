<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BotController extends Controller
{
    public function index()
    {
        $db = new DBManager();
        $response = $db->raw('SELECT * FROM Bot');
        $message = "";

        if (count($response['data']) > 0) {
            $message = "Obtencion exitosa, " . count($response['data']) . " registros encontrados";
        } else {
            $message = "Obtencion exitosa, no hay datos registrados";
        }
        
        $respuesta = [
            "success" => true,
            "message" => $message,
            "data" => $response['data']
         ];

         return response($respuesta);
    }

    /**
     * Get all plans.
     *
     * @param  string  $id
     * @return Response
     */
    public function showPlans()
    {

        $db = new DBManager();
        $response = $db->raw("SELECT pl.Servicio, pl.VARIANTE_PRODUCTO, pl.PLAN_A_PRESENTAR_EN_ROBOT, pl.DESCRIPCION, pl.PRECIO, pl.Estado FROM Plan pl WHERE pl.Estado LIKE 'A'");
        
        if (count($response['data']) > 0) {
            $message = "Obtencion exitosa, " . count($response['data']) . " registros encontrados";
        } else {
            $message = "Obtencion exitosa, no hay datos registrados";
        }

        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $response['data']
         ];

         return response($respuesta);
    }

    /**
     * Get all agencies.
     *
     * @param  string  $id
     * @return Response
     */
    public function showAgencias(Request $request)
    {
        $datos = $request->all();
        $cid = $datos['ciudad'];

        $db = new DBManager;

        $response = $db->raw("      SELECT cag.NombreCiudadAgencia, ag.NombreAgencia, ag.Direccion, ag.IdPropio, ag.Estado FROM CiudadAgencia cag
                                    INNER JOIN Agencia ag ON cag.IdCiudadAgencia = ag.IdCiudadAgencia
                                    WHERE cag.NombreCiudadAgencia LIKE '" . $cid . "' AND ag.Estado = 'A' AND ag.Tipo != 'ATC'");

        if (count($response['data']) == 0) {

            $response = $db->raw("  SELECT cag.NombreCiudadAgencia, ag.NombreAgencia, ag.Direccion, ag.IdPropio, ag.Estado FROM CiudadAgencia cag
                                    INNER JOIN Agencia ag ON cag.IdCiudadAgencia = ag.IdCiudadAgencia
                                    WHERE ag.Estado = 'A' AND ag.Tipo != 'ATC'");

        }
        
        $agencias = [];
        foreach($response['data'] as $row => $item){

            $preData = [
                "Id" => $item['IdPropio'],
                "Ciudad" => $item['NombreCiudadAgencia'],
                "Agencia" => $item['NombreAgencia'],
                "Direccion" => $item['Direccion'],
                "Estado" => $item['Estado'],
            ];
            array_push($agencias,$preData);

        }

        $respuesta = [

            "success" => true,
            "message" => "Agencias cargadas correctamente",
            "data" => $agencias

        ];
        
        return response($respuesta);
    }

    /**
     * Save a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeUser(Request $request)
    {
        $user = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('Usuario', $user, array_keys($user), false);
        $success = $resultado['success'];

        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }

        return response($resultado);
    }

    /**
     * Save a new user activity.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeUserRecord(Request $request)
    {
        $userRecord = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('RegistroUsuario', $userRecord, array_keys($userRecord), false);
        $success = $resultado['success'];

        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }

        return response($resultado);
    }

    /**
     * Save a new topic activity.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeTopicRecord(Request $request)
    {
        $topicRecord = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('RegistroTopico', $topicRecord, array_keys($topicRecord), false);
        $success = $resultado['success'];

        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }

        return response($resultado);
    }

    /**
     * Save a new global conversation.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeGlobalConversation(Request $request)
    {
        $conversation = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('ConversacionGlobal', $conversation, array_keys($conversation), false);
        $success = $resultado['success'];

        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }

        return response($resultado);
    }

    
    /**
     * Save a new topic conversation.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeTopicConversation(Request $request)
    {
        $conversation = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('ConversacionTopico', $conversation, array_keys($conversation), false);
        $success = $resultado['success'];
        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }
        return response($resultado);
    }

    /**
     * Save a new survey conversation.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeConversationSurvey(Request $request)
    {
        $survey = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('Encuesta', $survey, array_keys($survey), false);
        $success = $resultado['success'];
        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }
        return response($resultado);
    }

    /**
     * Save a new update info action.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeUpdateInfo(Request $request)
    {
        $action = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('ActualizarDatos', $action, array_keys($action), false);
        $success = $resultado['success'];

        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $db->commit();
        }

        return response($resultado);
    }

    /**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, string $id)
    {
        $user = $request->all();
        $db = new DBManager;
        $resultado = $db->update('usuarios', $user, [ 'id' => $id ]);
        return response($resultado);
    }

    /**
     * Register user activity.
     *
     * @param  Request  $request
     * @return Response
     */
    public function registerActivity(Request $request)
    {
        $datos = $request->all();
        $identificador = $datos['Identificador'];
        $db = new DBManager;

        $response = $db->raw("  SELECT act.Id, act.Identificador, act.Tipo, act.FechaEnvio
                                FROM Actividad act
                                WHERE act.Identificador LIKE '" . $identificador);

        if (count($response['data']) == 0) {

            $db->beginTransaction();
            $resultado = $db->insert('Actividad', $datos, array_keys($datos), false);
            $success = $resultado['success'];

            if (!$success) {
                $db->rollback();
            }
            if ($success) {
                $db->commit();
            }

        } else {

            $db->beginTransaction();
            $resultado = $db->update('Actividad', $datos, [ 'Identificador' => $datos['Identificador'], 'Tipo' => $datos['Tipo'] ]);
            $success = $resultado['success'];

            if (!$success) {
                $db->rollback();
            }
            if ($success) {
                $db->commit();
            }

        }

        return response($resultado);

    }

    /**
     * Register user activity.
     *
     * @param  Request  $request
     * @return Response
     */
    public function verifyChange(Request $request)
    {
        $datos = $request->all();
        $identificador = $datos['identificador'];
        $db = new DBManager;

        $response = $db->raw("  SELECT cam.Id, cam.Identificador, cam.Cantidad
                                FROM Cambio cam
                                WHERE cam.Identificador LIKE '" . $identificador);

        if (count($response['data']) == 0) {

            $db->beginTransaction();
            $resultado = $db->insert('Cambio', $datos, array_keys($datos), false);
            $success = $resultado['success'];

            $cambios = [];

            foreach($response['data'] as $row => $item){

                $preData = [
                    "Id" => $item['Id'],
                    "Identificador" => $item['Identificador'],
                    "Cantidad" => $item['Cantidad'],
                ];
                array_push($cambios,$preData);
    
            }

            if (!$success) {
                $db->rollback();
            }
            if ($success) {
                $db->commit();

                $respuesta = [
    
                    "success" => true,
                    "message" => "Cambio registrado",
                    "data" => $cambios
        
                ];
            }

        } else {

            $cantidad = $datos['Cantidad'] + $response['data']['Cantidad'];

            $db->beginTransaction();
            $resultado = $db->update('Cambio', $datos, [ 'Identificador' => $datos['Identificador'], 'Accion' => $datos['Accion'], 'Cantidad' => $cantidad ]);
            $success = $resultado['success'];

            $cambios = [];

            foreach($response['data'] as $row => $item){

                $preData = [
                    "Id" => $item['Id'],
                    "Identificador" => $item['Identificador'],
                    "Cantidad" => $item['Cantidad'],
                ];
                array_push($cambios,$preData);
    
            }

            if (!$success) {
                $db->rollback();
            }
            if ($success) {
                $db->commit();

                $respuesta = [
    
                    "success" => true,
                    "message" => "Cambio registrado",
                    "data" => $cambios
        
                ];
            }

        }

        return response($respuesta);
    }

    public function testValidator(Request $request) 
    {
        try {
            $valido = $this->validate($request, [
                'body' => 'required',
                'email' => 'email',
                'title' => 'required'
            ]);
        } catch(ValidationException $e) {
            return response(['success' => false, 'errors' => $e->errors()]);
        }

        return response($request->all());
    }
}
