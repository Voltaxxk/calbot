<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


require 'vendor/autoload.php';
// require 'vendor/phpmailer/phpmailer/src/Exception.php';
// require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
// require 'vendor/phpmailer/phpmailer/src/SMTP.php';

class MailController extends Controller
{
    public function index()
    {
        echo "Api Topicos Works!";
    }
    public function sendMail(Request $request){

        $postData = $request->all();
        
        try{
            $valido = $this->validate($request, [
                'email' => 'required'
            ]);
        }catch(ValidationException $e){

            return response(['sucess' => false, 'errors' => $e->errors()]);

        }
        

        $rand1 = rand(10,50);
        $rand2 = rand(51,99);
        $order = rand(0,1);

        if($order == 0){

            $numb = $rand1.$rand2;
        
        }else{

            $numb = $rand2.$rand1;

        }

        $userEmail = $postData['email'];

        $mail = new PHPMailer(true);

        try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'alfredogarzon1903@gmail.com';                     //SMTP username
        $mail->Password   = 'iivmhctwgdpvgrko';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        //Recipients
        $mail->setFrom('alfredogarzon1903@gmail.com', 'Admin Bot');
        $mail->addAddress($userEmail, 'Joe User');     //Add a recipient
        // $mail->addAddress('ellen@example.com');               //Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Se ha detectado que hubo un cambio en : ';
        $mail->Body    = 'Por favor ingrese el siguiente codigo de verificacion para validad su actualizacion: '.$numb;
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
        } catch (Exception $e) {
             echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

        
    }

}