<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

use function PHPSTORM_META\type;

date_default_timezone_set('America/Guayaquil');

class FilesController extends Controller
{

    public function index(){


        $message = "Ok Api Works!";
        $respuesta = [
            "success" => true,
            "message" => "Obtencion Servicios Exitosa",
            "data" => $message
        ];    
        return response($respuesta);

    }


    public function saveFiles() {
        $db = new DBManager;
        $countFiles = count($_FILES['files']['name']);

        //Definir carpeta de carga
        $upload_location_images = "public/images/";
        $upload_location_document = "public/documents/";

        $filesArr = array();

        // loop files
       
        for($index = 0; $index < $countFiles; $index++){

            $path = '';

            $uniqueSaveName = time().uniqid(rand());

            $fileName  = $_FILES['files']['name'][$index];

            $ext = pathinfo($fileName, PATHINFO_EXTENSION);

            $destineFile = $upload_location_images.$uniqueSaveName.'.'.$ext;

            $valid_ext = array("png","jpeg", "jpg");

            if(in_array($ext, $valid_ext)){

                $path = $destineFile;

            if(move_uploaded_file($_FILES['files']['tmp_name'][$index], $path)){

                $filesArr[] = $destineFile;

                }
            }
            
            $insert['TipoRecurso'] = 1;
            $insert['Path'] = $path;
            $inset['Estado'] = 'A';

            $db->beginTransaction();
            $result = $db->insert('Recursos', $insert, array_keys($insert), false);
            $success = $result['success'];

            if(!$success){
                $respuesta = [
                    "success" => true,
                    "message" => "Hubo un error al momento de guardar archivos",
                ];  

                return response($respuesta);

            }else {

                $db->commit();

            }
        };
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion Servicios Exitosa",
            "data" => $filesArr
        ];  

        return response($respuesta);
    }


    public function getImages(Request $request){


        $getRequest = $request->all();
        $tipoRecurso = $getRequest['tipoRecurso'];
     
        $db = new DBManager;

        $response = $db->raw("SELECT Id, TipoRecurso, Path FROM `Recursos` WHERE TipoRecurso = $tipoRecurso and Estado = 'A'");
        $success = $response['success'];

        $data = $response['data'];

        foreach($data as $key => $value){

            $data[$key]['Path'] = 'localhost/calbot/'. $value['Path'];

        }

        $respuesta = [
            "success" => true,
            "message" => "Obtencion Servicios Exitosa",
            "data" => $data
        ];  


        return response($respuesta);
    }


    public function deleteFile(Request $request){

        $datos = $request->all();
        $respuesta = [];
        $db = new DBManager;
        $db->beginTransaction();
        $response = $db->update(
            'Recursos',
            ["Estado" => $datos['Estado']],
            ["Id" => $datos['Id']],
            false
        );
        $success = $response['success'];
        if($success){

            $db->commit();
            $respuesta = [

                "success" => $success,
                "message" => "Archivo eliminado exitosamente"

            ];
        }else{
            $db->rollback();
            $respuesta = [

                "success" => $success,
                "message" => "Error al eliminar el archivo"

            ];
        }

        return response($respuesta);

    }

}