<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use function PHPSTORM_META\type;

date_default_timezone_set('America/Guayaquil');

class BotReports extends Controller
{
    public function index()
    {
        echo "Api Reportes Works";
    }


    /*********************************** Real time ************************************/

    /****************************** Tiempo Conversacion Global *******************************/

    public function globalBotTime (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionGlobal,FechaInicioConversacionGlobal)))) as promedioGlobal 
                              FROM `ConversacionGlobal` 
                              WHERE(FechaInicioConversacionGlobal BETWEEN '2021-05-30 00:00:00' AND '2021-05-30 23:59:59')");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionGlobal,FechaInicioConversacionGlobal)))) as promedioGlobal 
                              FROM `ConversacionGlobal` 
                              WHERE(FechaInicioConversacionGlobal BETWEEN '$realtimeStart' AND '$realtimeFinish')");

        $data = [];

    
        if(isset($response['data'][0]['promedioGlobal'])){

            $data['promedioGlobal'] = substr($response['data'][0]['promedioGlobal'], 0,8);

        }else{

            $data['promedioGlobal'] = '00:00:00';

        }

        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

    /****************************** Fin Tiempo Conversacion Global *******************************/



    /******************************Detalle de Facturas *******************************/

    public function billDetails (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Detalle Saldo Factura' OR 
                                     Topico LIKE 'Revisar Mis Saldos' OR 
                                     Topico LIKE 'Revisar mis Facturas (Facturas)' OR 
                                     Topico LIKE 'Ayuda para Entender mi Factura' OR 
                                     Topico LIKE 'Puntos de Pago')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                              GROUP BY Topico");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                            FROM `ConversacionTopico` 
                            WHERE (Topico LIKE 'Detalle Saldo Factura' OR 
                                   Topico LIKE 'Revisar Mis Saldos' OR 
                                   Topico LIKE 'Revisar mis Facturas (Facturas)' OR 
                                   Topico LIKE 'Ayuda para Entender mi Factura' OR 
                                   Topico LIKE 'Puntos de Pago')  AND 
                                   (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                            GROUP BY Topico"); 

         $responseChilds = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                     FROM `ConversacionTopico` 
                                     WHERE (Topico LIKE 'Revisar mis Facturas' OR 
                                            Topico LIKE 'Descripción Productos Contratados (Saldos)')  AND 
                                            (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                    GROUP BY Topico");

       

        $data = [];
        $dataChilds = [];

        foreach($responseChilds['data'] as $item){

            $dataChilds[$item['Topico']] = substr($item['promedio'], 0,8);

        }

        while(sizeof($dataChilds) <= 1){

            if(!array_key_exists('Descripción Productos Contratados (Saldos)', $dataChilds)){

                $dataChilds['Descripción Productos Contratados (Saldos)'] = '00:00:00';

            }else if(!array_key_exists('Revisar mis Facturas', $dataChilds)){

                $dataChilds['Revisar mis Facturas'] = '00:00:00';

            }

        }


        foreach($response['data'] as $item){
            
           if($item['Topico'] == 'Revisar Mis Saldos'){

                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                              "Hijos" => $dataChilds);

                
                $data[$item['Topico']] =  $nodo;
            
           }else{

           
                $data[$item['Topico']] = substr($item['promedio'], 0,8);

           }

        }

        while(sizeof($data) <= 4){

            if(!array_key_exists('Detalle Saldo Factura', $data)){

                $data['Detalle Saldo Factura'] = '00:00:00';

            } else if(!array_key_exists('Revisar Mis Saldos', $data)){

                $nodo = array("Revisar Mis Saldos" => "00:00:00",

                              "Hijos" => $dataChilds);

                $data['Revisar Mis Saldos'] = $nodo;

            }else if(!array_key_exists('Revisar mis Facturas (Facturas)', $data)){

                array($data['Revisar mis Facturas (Facturas)'] = '00:00:00');   

            }else if(!array_key_exists('Ayuda para Entender mi Factura', $data)){

                $data['Ayuda para Entender mi Factura'] = '00:00:00';

            }else if(!array_key_exists('Puntos de Pago', $data)){

                $data['Puntos de Pago'] = '00:00:00';

            }
        } 
        

        // print_r($data);


        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

     /****************************** Fin Detalle de Facturas *******************************/

    /****************************** Problemas Servicio *******************************/

    public function serviceProblems (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Problemas con el Servicio' OR 
                                     Topico LIKE 'Problemas con el Servicio de Internet' OR 
                                     Topico LIKE 'Problema Servicio de Televisión' OR
                                     Topico LIKE 'Problema servicio de Telefonía' OR 
                                     Topico LIKE 'Todos los Servicios' OR 
                                     Topico LIKE 'Visita Técnica')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                              GROUP BY Topico");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Problemas con el Servicio' OR 
                                     Topico LIKE 'Problemas con el Servicio de Internet' OR 
                                     Topico LIKE 'Problema Servicio de Televisión' OR
                                     Topico LIKE 'Problema servicio de Telefonía' OR 
                                     Topico LIKE 'Todos los Servicios' OR 
                                     Topico LIKE 'Visita Técnica')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                              GROUP BY Topico"); 

        $responseInter = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                   FROM `ConversacionTopico` 
                                   WHERE (Topico LIKE 'Mi Servicio de Internet está Lento' OR 
                                          Topico LIKE 'No Tengo Servicio de Internet')  AND 
                                        (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                   GROUP BY Topico");

        $responseTelev = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                   FROM `ConversacionTopico` 
                                   WHERE (Topico LIKE 'No tengo Audio/Video solucionado' OR 
                                          Topico LIKE 'Problemas con Control Remoto (Cambio de Pilas)')  AND 
                                         (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                   GROUP BY Topico");


        $responseVisit = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                   FROM `ConversacionTopico` 
                                   WHERE (Topico LIKE 'Cancelar visita Técnica (Cancelada)' OR 
                                          Topico LIKE 'Cancelar visita Técnica (No cancelada)')  AND 
                                         (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                   GROUP BY Topico");

        $data = [];
        $datChildsInter = [];
        $datChildsTelev = [];
        $datChildsVisit = [];

        /*** Problemas de internet ***/
        foreach($responseInter['data'] as $item){

            $datChildsInter[$item['Topico']] = substr($item['promedio'], 0,8);

        }

        while(sizeof($datChildsInter) <= 1){

            if(!array_key_exists('Mi Servicio de Internet está Lento', $datChildsInter)){

                $datChildsInter['Mi Servicio de Internet está Lento'] = '00:00:00';

            }else if(!array_key_exists('No Tengo Servicio de Internet', $datChildsInter)){

                $datChildsInter['No Tengo Servicio de Internet'] = '00:00:00';

            }

        }
        /*** Fin Problemas de internet ***/

        /*** Problemas de television ***/
        foreach($responseTelev['data'] as $item){

            $datChildsTelev[$item['Topico']] = substr($item['promedio'], 0,8);

        }

        while(sizeof($datChildsTelev) <= 1){

            if(!array_key_exists('No tengo Audio/Video solucionado', $datChildsTelev)){

                $datChildsTelev['No tengo Audio/Video solucionado'] = '00:00:00';

            }else if(!array_key_exists('Problemas con Control Remoto (Cambio de Pilas)', $datChildsTelev)){

                $datChildsTelev['Problemas con Control Remoto (Cambio de Pilas)'] = '00:00:00';

            }

        }
        /***Fin Problemas de television ***/

        /*** Problemas visita tecnica ***/
        foreach($responseVisit['data'] as $item){

            $datChildsVisit[$item['Topico']] = substr($item['promedio'], 0,8);

        }

        while(sizeof($datChildsVisit) <= 1){

            if(!array_key_exists('Cancelar visita Técnica (Cancelada)', $datChildsVisit)){

                $datChildsVisit['Cancelar visita Técnica (Cancelada)'] = '00:00:00';

            }else if(!array_key_exists('Cancelar visita Técnica (No cancelada)', $datChildsVisit)){

                $datChildsVisit['Cancelar visita Técnica (No cancelada)'] = '00:00:00';

            }

        }

        
        /*** Fin Problemas visita tecnica ***/


        /***Area de la data total ****/

        foreach($response['data'] as $item){
            
            if ($item['Topico'] == 'Problemas con el Servicio de Internet'){

                
                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                              "Hijos" => $datChildsInter);

                $data[$item['Topico']] =  $nodo;

            }else if($item['Topico'] == 'Problema Servicio de Televisión') {

                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                              "Hijos" => $datChildsTelev);

                $data[$item['Topico']] = $nodo;

            }else if($item['Topico'] == 'Visita Técnica') {

                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                              "Hijos" => $datChildsVisit);

                $data[$item['Topico']] = $nodo;    


            }else{

                $data[$item['Topico']] = substr($item['promedio'], 0,8);

            }

        }

        while(sizeof($data) <= 5){

            if(!array_key_exists('Problemas con el Servicio', $data)){

                $data['Problemas con el Servicio'] = '00:00:00';

            } else if(!array_key_exists('Problemas con el Servicio de Internet', $data)){

                $nodo = array("Problemas con el Servicio de Internet" => "00:00:00",

                              "Hijos" => $datChildsInter);
             
                $data['Problemas con el Servicio de Internet'] = $nodo;

            }else if(!array_key_exists('Problema Servicio de Televisión', $data)){

                $nodo = array("Problema Servicio de Televisión" => "00:00:00",

                              "Hijos" => $datChildsTelev);

                $data['Problema Servicio de Televisión'] = $nodo;   

            }else if(!array_key_exists('Problema servicio de Telefonía', $data)){

                $data['Problema servicio de Telefonía'] = '00:00:00';

            }else if(!array_key_exists('Todos los Servicios', $data)){

                $data['Todos los Servicios'] = '00:00:00';

            }else if(!array_key_exists('Visita Técnica', $data)){

                $nodo = array("Visita Técnica" => "00:00:00",

                              "Hijos" => $datChildsVisit);

                $data['Visita Técnica'] = $nodo;

            }
        } 

        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

     /****************************** Fin Problemas Servicio *******************************/
     

    /****************************** Descripcion de los productos Contratados *******************************/

    public function productsDescription (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Descripción Productos Contratados (Principal)')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                              GROUP BY Topico");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Descripción Productos Contratados (Principal)')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                              GROUP BY Topico"); 

        $data = [];


        foreach($response['data'] as $item){
            
           
            $data[$item['Topico']] = substr($item['promedio'], 0,8);

        }

        if(!array_key_exists('Descripción Productos Contratados (Principal)', $data)){

            $data['Descripción Productos Contratados (Principal)'] = '00:00:00';

        } 

        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

     /****************************** Fin Problemas Servicio *******************************/


     /****************************** Actualizar Datos *******************************/

    public function updateInfo (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Actualizar Datos de mi Cuenta' OR 
                                     Topico LIKE 'Actualizar Email' OR 
                                     Topico LIKE 'Actualizar Teléfonos' OR 
                                     Topico LIKE 'Actulizar Debitos Bancarios')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                              GROUP BY Topico");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Actualizar Datos de mi Cuenta' OR 
                                     Topico LIKE 'Actualizar Email' OR 
                                     Topico LIKE 'Actualizar Teléfonos' OR 
                                     Topico LIKE 'Actulizar Debitos Bancarios')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                              GROUP BY Topico"); 


        $responseChilds = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                    FROM `ConversacionTopico` 
                                    WHERE (Topico LIKE 'Confirmar Código Email')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                    GROUP BY Topico");

        $data = [];
        $datChildsUpdt = [];

      

        foreach($responseChilds['data'] as $item){
            
            $datChildsUpdt[$item['Topico']] = substr($item['promedio'], 0,8);

        }



        while(sizeof($datChildsUpdt) < 1){


            if(!array_key_exists('Confirmar Código Email', $datChildsUpdt)){

                $datChildsUpdt['Confirmar Código Email'] = '00:00:00';
            }

        } 


        foreach($response['data'] as $item){
            
            if($item['Topico'] == 'Actualizar Email'){

                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                                
                "Hijos" => $datChildsUpdt);

                $data[$item['Topico']] = $nodo;

            } else if($item['Topico'] == 'Actualizar Teléfonos') {

                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                                
                              "Hijos" => $datChildsUpdt);

                $data[$item['Topico']] = $nodo;
            } else{

               $data[$item['Topico']] = substr($item['promedio'], 0,8);
            }
        }

       while(sizeof($data) <= 3){


            if(!array_key_exists('Actualizar Datos de mi Cuenta', $data)){

                $data['Actualizar Datos de mi Cuenta'] = '00:00:00';
    
            } else if(!array_key_exists('Actualizar Email', $data)) {

                $nodo = array("Actualizar Email" => "00:00:00",

                              "Hijos" => $datChildsUpdt);

                $data['Actualizar Email'] = $nodo;

            } else if(!array_key_exists('Actualizar Teléfonos', $data)) {

                $nodo = array("Actualizar Teléfonos" => "00:00:00",

                              "Hijos" => $datChildsUpdt);

                $data['Actualizar Teléfonos'] = $nodo;

            } else if(!array_key_exists('Actulizar Debitos Bancarios', $data)){

                $data['Actulizar Debitos Bancarios'] = '00:00:00';
    
            }

        } 

        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

    /****************************** Fin Actualizar Datos *******************************/

    /****************************** Instructivos *******************************/

    public function instructive (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Instructivos' OR 
                                     Topico LIKE 'WiFi Monitor' OR 
                                     Topico LIKE 'Problemas con Control Remoto (Cambio de Pilas)')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                              GROUP BY Topico");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Instructivos' OR 
                                     Topico LIKE 'Wifi Monitor' OR 
                                     Topico LIKE 'Control Remoto' OR)  AND 
                                     (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                              GROUP BY Topico"); 



        $data = [];


        foreach($response['data'] as $item){
            
            $data[$item['Topico']] = substr($item['promedio'], 0,8);

        }

       while(sizeof($data) <= 2){


            if(!array_key_exists('Instructivos', $data)){

                $data['Instructivos'] = '00:00:00';
    
            } else if(!array_key_exists('WiFi Monitor', $data)) {

                $data['WiFi Monitor'] = '00:00:00';

            } else if(!array_key_exists('Problemas con Control Remoto (Cambio de Pilas)', $data)) {

                $data['Problemas con Control Remoto (Cambio de Pilas)'] = '00:00:00';

            } 

        } 

        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

     /****************************** Fin Instuctivos *******************************/

    /****************************** Modificar Servicio Contratado *******************************/

    public function changeServices (){

        $realtimeFinish = date('Y-m-d H:i:s');
        $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';

        $db = new DBManager();
        $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Modificar Servicios Contratados' OR 
                                     Topico LIKE 'Activar o Agregar nuevos productos' OR 
                                     Topico LIKE 'Desactivar servicios o productos' OR 
                                     Topico LIKE 'Cambio de Direccion')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                              GROUP BY Topico");*/

                            ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                              FROM `ConversacionTopico` 
                              WHERE (Topico LIKE 'Modificar Servicios Contratados' OR 
                                     Topico LIKE 'Activar o Agregar Nue%' OR 
                                     Topico LIKE 'Desactivar servicios o productos' OR 
                                     Topico LIKE 'Cambio de Direccion')  AND 
                                     (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                              GROUP BY Topico");

        $responseChilds = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                    FROM `ConversacionTopico` 
                                    WHERE (Topico LIKE 'Desactivación de servicios (Confirmación de EMail)')  AND 
                                        (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                    GROUP BY Topico");                      

        $responseKeepAc = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                    FROM `ConversacionTopico` 
                                    WHERE (Topico LIKE 'Desactivar producto (Mantener el producto activo)')  AND 
                                            (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                    GROUP BY Topico");
        $data = [];
        $datChildsUpdt = [];

        foreach($responseChilds['data'] as $item){
            
            $datChildsUpdt[$item['Topico']] = substr($item['promedio'], 0,8);

        }

        foreach($responseKeepAc['data'] as $item){
            
            $datChildsUpdt[$item['Topico']] = substr($item['promedio'], 0,8);

        }

     

        while(sizeof($datChildsUpdt) <= 1){

            
            if(!array_key_exists('Desactivación de servicios (Confirmación de EMail)', $datChildsUpdt)){

                $datChildsUpdt['Desactivación de servicios (Confirmación de EMail)'] = '00:00:00';

            }else if(!array_key_exists('Desactivar producto (Mantener el producto activo)', $datChildsUpdt)){

                $datChildsUpdt['Desactivar producto (Mantener el producto activo)'] = '00:00:00';
            }    
        }

        /*** Inicio del Array Data  ***/

        foreach($response['data'] as $item){
            
            if($item['Topico'] == 'Desactivar servicios o productos'){



                $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                                
                "Hijos" => $datChildsUpdt);

                $data[$item['Topico']] = $nodo;

            }else{

                $data[$item['Topico']] = substr($item['promedio'], 0,8);

            }

        }

       while(sizeof($data) <= 3){

            if(!array_key_exists('Modificar Servicios Contratados', $data)){

                $data['Modificar Servicios Contratados'] = '00:00:00';
    
            } elseif(!array_key_exists('Activar o Agregar nuevos productos', $data)) {

                $data['Activar o Agregar nuevos productos'] = '00:00:00';

            } else if(!array_key_exists('Desactivar servicios o productos', $data)) {

                $nodo = array("Desactivar servicios o productos" => "00:00:00",

                "Hijos" => $datChildsUpdt);

                $data['Desactivar servicios o productos'] = $nodo;

            } else if(!array_key_exists('Cambio de Direccion', $data)){

                $data['Cambio de Direccion'] = '00:00:00';
    
            }

        }

        /*** FIN del array Data ***/

        $respuesta = [
                "success" => true,
                "message" => "Obtencion exitosa",
                "data" => $data
            ];                

        return response($respuesta);
    }

    /****************************** Fin Modificar servicio contratado *******************************/

    /****************************** XTRIM/HBO *******************************/

        public function xTrimHbo (){

            $realtimeFinish = date('Y-m-d H:i:s');
            $realtimeStart = substr($realtimeFinish,0,10) . ' 00:00:00';
    
            $db = new DBManager();
            $response = $db->raw/*("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                  FROM `ConversacionTopico` 
                                  WHERE (Topico LIKE 'Contratar / Promociones' OR 
                                         Topico LIKE 'Contratar XTRIM Net' OR 
                                         Topico LIKE 'Contratar Canal de Futbol' OR 
                                         Topico LIKE 'Activar HBO')  AND 
                                         (FechaInicioConversacionTopico BETWEEN '2021-05-27 00:00:00' AND '2021-05-27 23:59:59')
                                  GROUP BY Topico");*/
    
                                ("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                  FROM `ConversacionTopico` 
                                  WHERE (Topico LIKE 'XTRIM/Canal del Fútbol/HBO' OR 
                                         Topico LIKE 'Contratar XTRIM' OR 
                                         Topico LIKE 'Contratar canal de Futbol' OR 
                                         Topico LIKE 'Activar HBO')  AND 
                                         (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                  GROUP BY Topico"); 

            $responseX = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                   FROM `ConversacionTopico` 
                                   WHERE (Topico LIKE 'Contratar XTRIM Net')  AND 
                                        (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                   GROUP BY Topico");


            $responseF = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                   FROM `ConversacionTopico` 
                                   WHERE (Topico LIKE 'Contratar Canal de Futbol')  AND 
                                        (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                   GROUP BY Topico");

            $responseH = $db->raw("SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(FechaCierreConversacionTopico,FechaInicioConversacionTopico)))) as promedio, Topico 
                                   FROM `ConversacionTopico` 
                                   WHERE (Topico LIKE 'Activar HBO')  AND 
                                        (FechaInicioConversacionTopico BETWEEN '$realtimeStart' AND '$realtimeFinish')
                                   GROUP BY Topico");
            $data = [];
            $datChildX = [];
            $datChildF = [];
            $datChildH = [];
            
            /*** XTRIM ***/
            foreach($responseX['data'] as $item){
                
                $datChildX[$item['Topico']] = substr($item['promedio'], 0,8);
    
            }

            while(sizeof($datChildX) < 1){


                if(!array_key_exists('Contratar XTRIM Net', $datChildX)){
    
                    $datChildX['Contratar XTRIM Net'] = '00:00:00';
                }
    
            } 
            /*** FIN XTRIM ***/

            /*** Futbol ***/
            foreach($responseF['data'] as $item){
    
                $datChildF[$item['Topico']] = substr($item['promedio'], 0,8);
    
            }

            while(sizeof($datChildF) < 1){


                if(!array_key_exists('Contratar Canal de Futbol', $datChildF)){
    
                    $datChildF['Contratar Canal de Futbol'] = '00:00:00';
                }
    
            } 
            /*** FIN Futbol ***/

            /*** HBO ***/
            foreach($responseH['data'] as $item){
    
                $datChildH[$item['Topico']] = substr($item['promedio'], 0,8);
    
            }

            while(sizeof($datChildH) < 1){


                if(!array_key_exists('Activar HBO', $datChildH)){
    
                    $datChildH['Activar HBO'] = '00:00:00';
                }
    
            } 
            /*** FIN HBO ***/


            foreach($response['data'] as $item){

                if($item['Topico'] == 'Contratar XTRIM Net'){

               
                    $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                                  "Hijos" => $datChildX);

                    $data[$item['Topico']] =  $nodo;          

                }else if($item['Topico'] == 'Contratar Canal de Futbol'){

                    $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                                  "Hijos" => $datChildF);

                     $data[$item['Topico']] =  $nodo;    

                }else if($item['Topico'] == 'Activar HBO'){

                    $nodo = array($item['Topico'] => substr($item['promedio'], 0,8),
                
                                  "Hijos" => $datChildH);

                    $data[$item['Topico']] =  $nodo;    

                }else {
                
                    $data[$item['Topico']] = substr($item['promedio'], 0,8);
                 }
    
            }
    
           while(sizeof($data) <= 3){
    
    
                if(!array_key_exists('Contratar / Promociones', $data)){
    
                    $data['Contratar / Promociones'] = '00:00:00';
        
                } else if(!array_key_exists('Contratar XTRIM Net', $data)) {
    
                    $nodo = array("Contratar XTRIM Net" => "00:00:00",

                                    "Hijos" => $datChildX);

                    $data['Contratar XTRIM Net'] = $nodo;
    
                } else if(!array_key_exists('Contratar Canal de Futbol', $data)) {
    
                    $nodo = array("Contratar Canal de Futbol" => "00:00:00",

                                    "Hijos" => $datChildF);

                    $data['Contratar Canal de Futbol'] = $nodo;
    
                } else if(!array_key_exists('Activar HBO', $data)){
    
                    $nodo = array("Activar HBO" => "00:00:00",

                                  "Hijos" => $datChildF);

                    $data['Activar HBO'] = $nodo;
        
                }
    
            } 
    
            $respuesta = [
                    "success" => true,
                    "message" => "Obtencion exitosa",
                    "data" => $data
                ];                
    
            return response($respuesta);
        }
    
    /****************************** Fin XTRIM/HBO *******************************/
}
