<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MensajesRecibidosController extends Controller
{
    /**
     * Save a new global conversation.
     *
     * @param  Request  $request
     * @return Response
     */
    public function searchAndStoreMessage(Request $request)
    {
        $consulta = $request->all();
        $db = new DBManager;
        $response = $db->raw("SELECT * FROM MensajeRecibido WHERE IdRecibido = ?", [$consulta['IdRecibido']]);
        
        $existeMensaje = false;

        if (count($response['data']) > 0) {
            $existeMensaje = true;
        } else {
            
            $dataInsertar = [
                'IdRecibido' => $consulta['IdRecibido']
            ];
            $db->beginTransaction();
            $resultado = $db->insert('MensajeRecibido', $dataInsertar, array_keys($dataInsertar), false);
            $success = $resultado['success'];

            if (!$success) {
                $db->rollback();
            }
            if ($success) {
                $db->commit();
            }
        }

        return response(['existeMensaje' => $existeMensaje]);
    }
}
