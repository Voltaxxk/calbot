<?php

namespace App\Controllers;

use App\DB\DBManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RespaldoBotController extends Controller
{
    public function index()
    {

        $db = new DBManager();
        $response = $db->raw('SELECT * FROM bot');
        
        $respuesta = [
            "success" => true,
            "message" => "Obtencion exitosa",
            "data" => $response['data'][0]
         ];
         return response($respuesta);
    }

    /**
     * Get the specified bot.
     *
     * @param  string  $id
     * @return Response
     */
    /*public function show($id)
    {

        $idTopico = $id;
        $db = new DBManager;
        $data = [];

        $contestacion = $db->raw('SELECT frase FROM contestacion WHERE idTopico = ? AND status = "A"',[$idTopico]);
        array_push($data,$contestacion);
        $claveUno = $db->raw('SELECT palabra FROM claveuno WHERE idTopico = ? AND status = "A"',[$idTopico]);
        array_push($claveUno,$contestacion);
        $claveUno = $db->raw('SELECT palabra FROM clavedos WHERE idTopico = ? AND status = "A"',[$idTopico]);
        array_push($claveDos,$contestacion);

        return response($data);
    }*/


    /**
     * Obtener los saludos.
     *
     * @param  string  $id
     * @return Response
     */
    public function saludos($id)
    {

        $idBot = $id;
        $db = new DBManager;
        $response = $db->raw('SELECT * FROM saludo WHERE idBot = ? and Estado = ? ', [$idBot,"A"]);
        $saludos = [];
        foreach($response['data'] as $row => $item){

            $preData = [

                "idSaludo" => $item['IdSaludo'],
                "idBot" => $item['IdBot'],
                "Saludo" => $item['Frase'],
            ];
            array_push($saludos,$preData);

        }

        $respuesta = [

            "success" => true,
            "message" => "Saludos cargados correctamente",
            "data" => $saludos

        ];
        
        return response($respuesta);


    }

    /**
     * Obtener los despedida
     *
     * @param  string  $id
     * @return Response
     */
    public function despedida($id)
    {
        $idBot = $id;
        $db = new DBManager;
        $response = $db->raw('SELECT * FROM despedida WHERE idBot = ? and Estado = ? ', [$idBot,"A"]);
        $saludos = [];
        foreach($response['data'] as $row => $item){

            $preData = [

                "idDespedida" => $item['IdDespedida'],
                "idBot" => $item['IdBot'],
                "Despedida" => $item['Frase'],
            ];
            array_push($saludos,$preData);

        }

        $respuesta = [

            "success" => true,
            "message" => "Despedidas cargadas correctamente",
            "data" => $saludos

        ];
        
      return response($respuesta);

    }


    /**
     * Save a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = $request->all();
        $db = new DBManager;
        $db->beginTransaction();
        $resultado = $db->insert('usuarios', $user, array_keys($user), false);
        $success = $resultado['success'];
        if (!$success) {
            $db->rollback();
        }
        if ($success) {
            $resultad2 = $db->insert('roles', ['id' => '2'], ['id'], false);
            $success = $resultad2['success'];
        }
        if ($success) {
            $db->commit();
        }
        return response($resultado);
    }

    /**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, string $id)
    {
        $user = $request->all();
        $db = new DBManager;
        $resultado = $db->update('usuarios', $user, [ 'id' => $id ]);
        return response($resultado);
    }

    public function testValidator(Request $request) 
    {
        try {
            $valido = $this->validate($request, [
                'body' => 'required',
                'email' => 'email',
                'title' => 'required'
            ]);
        } catch(ValidationException $e) {
            return response(['success' => false, 'errors' => $e->errors()]);
        }

        return response($request->all());
    }
}
