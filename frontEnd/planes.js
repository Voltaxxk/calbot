var urlBase = "http://localhost/calbot/api/adminBot";
var headers = {
    'Access-Control-Allow-Origin': '*'
};

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


$(document).ready(function() {
    getDataDashboard();
});

let logout = document.getElementById("logout")
logout.addEventListener("click", function() {

    window.location = "index.html";

});


var servicio = getParameterByName('nombre');
var table

let toolTip = document.getElementById("añadirServicio")
let toolOptions = {

    'enterDelay': 1000,
}
let toolTipInstance = M.Tooltip.init(toolTip, toolOptions);

let sideBar = document.getElementById('slide-out');
let sidebarOptions = {

    'draggable': true,

}
let sideInstance = M.Sidenav.init(sideBar, sidebarOptions);

let icono = document.getElementById('menu');
icono.addEventListener('click', function() {

    sideInstance.open();

})


let enviar = document.getElementById("enviarEdit");

enviar.addEventListener("click", async function() {
    await editarData();
    modal1Instance.close();
    $('#Editar')[0].reset();
    M.updateTextFields();
    getDataDashboard();
});

let eliminar = document.getElementById("eliminarServicio");

eliminar.addEventListener("click", async function() {
    await eliminarData();
    modal2Instance.close();
    document.getElementById("Eliminar").reset();
    M.updateTextFields();
    getDataDashboard();
});

let añadir = document.getElementById("añadirServicio");
añadir.addEventListener("click", function() {
    clearInputs();

    document.getElementById("addServicio").setAttribute("style", "");
    document.getElementById("enviarEdit").setAttribute("style", "display: none;");
    document.getElementById("titleAction").innerHTML = "Añadir Servicio";

    $('#Editar')[0].reset();
    M.updateTextFields();
    modal1Instance.open();
});

let addServicio = document.getElementById("addServicio");
addServicio.addEventListener("click", async function() {
    await añadirServicio();
    modal1Instance.close();
    $('#Editar')[0].reset();
    M.updateTextFields();
});

var modal1 = document.getElementById('modal1');
var modal1Instance = M.Modal.init(modal1);


var modal2 = document.getElementById('modal2');
var modal2Instance = M.Modal.init(modal2);


var formEdit = document.getElementById("Editar");

async function getDataDashboard() {

    let apiFix = urlBase + `/planesServicios?nombre=${servicio}`;
    document.getElementById('dashboardInfo').innerHTML = `Dashboard Servicio: ${servicio}`;

    if ($("#tbody").length == 0) {

        console.log("No existen datos que mostrar");

    } else {

        $("#table").DataTable().destroy();

    }

    table = $("#table").DataTable({

        "pageLength": 6,
        "ordering": false,

        "ajax": {
            "url": `${apiFix}`,
            "method": "GET"

        },

        "columns": [
            { "data": "IdPlan" },
            { "data": "tipoServicio" },
            { "data": "tipoPlan" },
            { "data": "descripcion" },
            { "data": "PRECIO" },
            { "defaultContent": "<button class='btn blue accent-2' id='editar'><span class='material-icons table'>edit</span></button><button type='button' class='btn red accent-2' id='eliminar'><span class='material-icons table'>delete</span></button>" }


        ],
    });
}

$(document).on("click", "#editar", async function(e) {

    document.getElementById("enviarEdit").setAttribute("style", "");
    document.getElementById("addServicio").setAttribute("style", "display: none;")
    document.getElementById("titleAction").innerHTML = "Editar Servicio";
    let data = table.row($(this).parents("tr")).data();

    let preData = new Array();
    preData["IdPlan"] = data['IdPlan'];
    preData["Servicio"] = servicio;
    preData["VARIANTE_PRODUCTO"] = data['tipoServicio'];
    preData["PLAN_A_PRESENTAR_EN_ROBOT"] = data['tipoPlan'];
    preData["DESCRIPCION"] = data['descripcion'];
    preData["PRECIO"] = data['PRECIO'];


    document.getElementById('Servicio').setAttribute("value", data['tipoServicio']);
    document.getElementById('Plan').setAttribute("value", data['tipoPlan']);
    document.getElementById('Descripcion').setAttribute("value", data['descripcion']);
    document.getElementById('Precio').setAttribute("value", data['PRECIO']);
    document.getElementById('IdPlan').setAttribute("value", data['IdPlan']);

    modal1Instance.open();

});

$(tbody).on("click", "button#eliminar", function() {

    let data = table.row($(this).parents("tr")).data();
    document.getElementById('infoEliminar').innerHTML = `¿Esta completamente seguro de que desea eliminar el Servicio: ${data['tipoServicio']}`;
    document.getElementById('ServicioEliminar').setAttribute("value", data['IdPlan']);
    modal2Instance.open();

});


async function editarData() {
    let apiFix = urlBase + "/editServicio";
    let data = [];
    data["IdPlan"] = document.querySelector('#IdPlan').value;
    data["Servicio"] = servicio;
    data["VARIANTE_PRODUCTO"] = document.querySelector('#Servicio').value;
    data["PLAN_A_PRESENTAR_EN_ROBOT"] = document.querySelector('#Plan').value;
    data["DESCRIPCION"] = document.querySelector('#Descripcion').value;
    data["PRECIO"] = document.querySelector('#Precio').value;

    await $.ajax({
        method: "POST",
        url: apiFix,
        data: {
            "IdPlan": data['IdPlan'],
            "Servicio": data['Servicio'],
            "VARIANTE_PRODUCTO": data['VARIANTE_PRODUCTO'],
            "PLAN_A_PRESENTAR_EN_ROBOT": data['PLAN_A_PRESENTAR_EN_ROBOT'],
            "DESCRIPCION": data['DESCRIPCION'],
            "PRECIO": data['PRECIO']
        },
        dataType: "json",
        headers: headers,
    }).done(function(response) {
        Swal.fire(
            'Exitoso',
            'Se ha editado correctamente el servicio',
            'success'
        )
    });
}

async function eliminarData() {
    let apiFix = urlBase + "/editServicio";
    let IdPlan = document.getElementById("ServicioEliminar").value

    await $.ajax({
        method: "POST",
        url: apiFix,
        data: {
            "IdPlan": IdPlan,
            "Estado": "I"
        },
        dataType: "json",
        headers: headers,
    }).done(function(response) {

        Swal.fire(
            'Exitoso',
            'Se ha Eliminado correctamente el servicio',
            'success'
        )
    });
}


async function añadirServicio() {

    let apiFix = urlBase + "/nuevoServicio";
    let data = [];
    data["Servicio"] = servicio;
    data["VARIANTE_PRODUCTO"] = document.querySelector('#Servicio').value;
    data["PLAN_A_PRESENTAR_EN_ROBOT"] = document.querySelector('#Plan').value;
    data["DESCRIPCION"] = document.querySelector('#Descripcion').value;
    data["PRECIO"] = document.querySelector('#Precio').value;

    if ((data["Servicio"] && data["VARIANTE_PRODUCTO"] && data["PLAN_A_PRESENTAR_EN_ROBOT"] && data["DESCRIPCION"] && data["PRECIO"]) == "") {

        Swal.fire({
            icon: 'error',
            title: 'No se ha ingresado todos los datos',
            text: 'Porfavor ingrese correctamente los campos',
        })

    } else if (data["VARIANTE_PRODUCTO"] == "") {

        Swal.fire({
            icon: 'error',
            title: 'No ha ingresado Tipo de servicio',
            text: 'Porfavor intente nuevamente',
        })

    } else if (data["PLAN_A_PRESENTAR_EN_ROBOT"] == "") {

        Swal.fire({
            icon: 'error',
            title: 'No ha ingresado tipo del plan',
            text: 'Porfavor intente nuevamente',
        })

    } else {

        await $.ajax({
            method: "POST",
            url: apiFix,
            data: {
                "Servicio": data["Servicio"],
                "VARIANTE_PRODUCTO": data["VARIANTE_PRODUCTO"],
                "PLAN_A_PRESENTAR_EN_ROBOT": data["PLAN_A_PRESENTAR_EN_ROBOT"],
                "DESCRIPCION": data["DESCRIPCION"],
                "PRECIO": data["PRECIO"],
            },
            dataType: "json",
            headers: headers,
        }).done(function(response) {
            getDataDashboard();
            Swal.fire(
                'Exitoso',
                'Se ha ingresado el nuevo servicio correctamente.',
                'success'
            )
        });

    }
}


function clearInputs() {
    document.getElementById("Servicio").setAttribute("value", "");
    document.getElementById("Plan").setAttribute("value", "");
    document.getElementById("Descripcion").setAttribute("value", "");
    document.getElementById("Precio").setAttribute("value", "");
}