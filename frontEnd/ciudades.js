var urlBase = "http://localhost/calbot/api/adminBot";
var headers = {
    'Access-Control-Allow-Origin': '*'
};


document.addEventListener("DOMContentLoaded", async function() {

    await getDataServicios();
    getButtonsData();
    var elems = document.querySelectorAll('.scrollspy');
    var instances = M.ScrollSpy.init(elems);
});

let logout = document.getElementById("logout")
logout.addEventListener("click", function() {

    window.location = "index.html";

});


let iconSize = 'md-40';

let cardContainer = document.getElementById('cardsContainer');

let sideBar = document.getElementById('slide-out');
let sidebarOptions = {

    'draggable': true,

}
let sideInstance = M.Sidenav.init(sideBar, sidebarOptions);



let icono = document.getElementById('menu');
icono.addEventListener('click', function() {

    sideInstance.open();
})

async function getDataServicios() {

    let apiFix = urlBase + "/ciudades";
    let data;

    await $.ajax({
        method: "GET",
        url: apiFix,
        headers: headers,
        processData: false,
        contentType: false,
    }).done(function(response) {

        data = Object.values(response.data)

    });

    console.log(data);
    Object.entries(data).forEach(([key, value]) => {

        let nombreAge = value['nombreAgencia'];
        let idAgencia = value['idAgencia'];

        let itemCard = `<div class="card small">
                            <div class="card-content ">
                                <span class="card-title">${nombreAge}</span>
                                <span class="material-icons md-40">villa</span>
                                <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                            </div>
                            <div class="card-action">
                                <a class="btn-floating halfway-fab waves-effect waves-light green darken-4" id="${idAgencia}" nombreagencia="${nombreAge}"><i class="material-icons">visibility</i></a>
                            </div>
                        </div>`;


        cardContainer.insertAdjacentHTML('beforeend', itemCard)

    });

}

function getButtonsData() {

    const botones = document.querySelectorAll('.btn-floating');

    botones.forEach(boton => {
        boton.addEventListener("click", function() {
            // console.log("Se ha dado click a: " + this.id);
            // console.log("El nombre de la agencia es: " + this.getAttribute("nombreagencia"));
            window.location = "agencias.html?idAgencia=" + this.id + "&nombAgencia=" + this.getAttribute("nombreagencia");
        })

    })

}