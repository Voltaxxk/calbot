var urlBase = "http://localhost/calbot/api/adminBot";
var headers = {
    'Access-Control-Allow-Origin': '*'
};

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


$(document).ready(function() {
    getDataDashboard();
    setIdAgenciaCiudad();
});


let logout = document.getElementById("logout")
logout.addEventListener("click", function() {

    window.location = "index.html";

});


var servicio = getParameterByName('idAgencia');
var nombAgencia = getParameterByName('nombAgencia');
var table;

let toolTip = document.getElementById("añadirServicio")
let toolOptions = {

    'enterDelay': 1000,
}
let toolTipInstance = M.Tooltip.init(toolTip, toolOptions);

let sideBar = document.getElementById('slide-out');
let sidebarOptions = {

    'draggable': true,

}
let sideInstance = M.Sidenav.init(sideBar, sidebarOptions);

let icono = document.getElementById('menu');
icono.addEventListener('click', function() {

    sideInstance.open();

})


let enviar = document.getElementById("enviarEdit");

enviar.addEventListener("click", async function() {
    await editarData();
    modal1Instance.close();
    $('#Editar')[0].reset();
    M.updateTextFields();
    getDataDashboard();
});

let eliminar = document.getElementById("eliminarServicio");

eliminar.addEventListener("click", async function() {
    await eliminarData();
    modal2Instance.close();
    document.getElementById("Eliminar").reset();
    M.updateTextFields();
    getDataDashboard();
});

let añadir = document.getElementById("añadirServicio");
añadir.addEventListener("click", function() {
    clearInputs();
    $('#Editar')[0].reset();
    M.updateTextFields();
    document.getElementById("addServicio").setAttribute("style", "");
    document.getElementById("enviarEdit").setAttribute("style", "display: none;");
    document.getElementById("titleAction").innerHTML = "Añadir Servicio";

    document.getElementById("Ciudad").setAttribute("value", nombAgencia);
    modal1Instance.open();
});

let addServicio = document.getElementById("addServicio");
addServicio.addEventListener("click", async function() {
    await añadirAgencia();
    modal1Instance.close();
    $('#Editar')[0].reset();
    M.updateTextFields();
    getDataDashboard();
});

var modal1 = document.getElementById('modal1');
var modal1Instance = M.Modal.init(modal1);


var modal2 = document.getElementById('modal2');
var modal2Instance = M.Modal.init(modal2);


var formEdit = document.getElementById("Editar");

async function getDataDashboard() {

    let apiFix = urlBase + `/agenciasCiudades?id=${servicio}`;
    document.getElementById('dashboardInfo').innerHTML = `Agencias de la ciudad: ${nombAgencia}`;

    if ($("#tbody").length == 0) {

        console.log("No existen datos que mostrar");

    } else {

        $("#table").DataTable().destroy();

    }

    table = $("#table").DataTable({

        responsive: true,
        "pageLength": 6,

        "ajax": {
            "url": `${apiFix}`,
            "method": "GET"

        },

        "columns": [
            { "data": "idAgencia" },
            { "data": "nombreCiudad" },
            { "data": "nombreAgencia" },
            { "defaultContent": "<button class='btn blue accent-2' id='editar'><span class='material-icons table'>edit</span></button><button type='button' class='btn red accent-2' id='eliminar'><span class='material-icons table'>delete</span></button>" }


        ],
    });
}

$(document).on("click", "#editar", async function(e) {

    document.getElementById("enviarEdit").setAttribute("style", "");
    document.getElementById("addServicio").setAttribute("style", "display: none;")
    document.getElementById("titleAction").innerHTML = "Editar Agencia";
    let data = table.row($(this).parents("tr")).data();

    document.getElementById('Ciudad').setAttribute("value", data['nombreCiudad']);
    document.getElementById('nombAgencia').setAttribute("value", data['nombreAgencia']);
    document.getElementById('IdAgencia').setAttribute("value", data['idAgencia']);


    modal1Instance.open();

});

$(tbody).on("click", "button#eliminar", function() {

    let data = table.row($(this).parents("tr")).data();
    document.getElementById('infoEliminar').innerHTML = `¿Esta completamente seguro de que desea eliminar la Agencia: ${data['nombreAgencia']}`;
    document.getElementById('AgenciaEliminar').setAttribute("value", data['idAgencia']);
    modal2Instance.open();

});


async function editarData() {
    let apiFix = urlBase + "/editAgencias";
    let data = [];
    data["IdAgencia"] = document.querySelector('#IdAgencia').value;
    data["NombreAgencia"] = document.querySelector('#nombAgencia').value;

    await $.ajax({
        method: "POST",
        url: apiFix,
        data: {
            "IdAgencia": data['IdAgencia'],
            "NombreAgencia": data['NombreAgencia'],
        },
        dataType: "json",
        headers: headers,
    }).done(function(response) {
        Swal.fire(
            'Exitoso',
            'Se ha editado correctamente el servicio',
            'success'
        )
    });
}

async function eliminarData() {
    let apiFix = urlBase + "/editAgencias";
    let IdAgencia = document.getElementById("AgenciaEliminar").value

    await $.ajax({
        method: "POST",
        url: apiFix,
        data: {
            "IdAgencia": IdAgencia,
            "Estado": "I"
        },
        dataType: "json",
        headers: headers,
    }).done(function(response) {

        Swal.fire(
            'Exitoso',
            'Se ha Eliminado correctamente el servicio',
            'success'
        )
    });
}


async function añadirAgencia() {

    let apiFix = urlBase + "/nuevaAgencia";
    let data = [];
    data["IdCiudadAgencia"] = document.querySelector("#IdCiudadAgencia").value;
    data["NombreAgencia"] = document.querySelector('#nombAgencia').value;

    if ((data["IdCiudadAgencia"]) == "") {

        Swal.fire({
            icon: 'error',
            title: 'No se ha ingresado todos los datos',
            text: 'Porfavor ingrese correctamente los campos',
        })

    } else if (data["NombreAgencia"] == "") {

        Swal.fire({
            icon: 'error',
            title: 'No ha ingresado el nombre de la agencia.',
            text: 'Porfavor intente nuevamente',
        })

    } else {

        await $.ajax({
            method: "POST",
            url: apiFix,
            data: {
                "IdCiudadAgencia": data["IdCiudadAgencia"],
                "NombreAgencia": data["NombreAgencia"],
            },
            dataType: "json",
            headers: headers,
        }).done(function(response) {
            Swal.fire(
                'Exitoso',
                'Se ha ingresado el nuevo servicio correctamente.',
                'success'
            )
        });

    }
}


function clearInputs() {
    document.getElementById("nombAgencia").setAttribute("value", "");
}

async function setIdAgenciaCiudad() {
    let apiFix = urlBase + `/agenciasCiudades?id=${servicio}`;
    let data = [];

    await $.ajax({
        method: "GET",
        url: apiFix,
        headers: headers,
        processData: false,
        contentType: false,
    }).done(function(response) {
        data = Object.values(response['data']);
    });

    document.getElementById("IdCiudadAgencia").setAttribute("value", data[0]['IdAgenciaCiudad']);
}