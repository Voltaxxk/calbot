var urlBase = "http://localhost/calbot/api/files";
var headers = {
    'Access-Control-Allow-Origin': '*'
};

var progressBar = document.getElementById("progressBar");
var idFile

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var tipoRecurso = getParameterByName("tipoRecurso");

document.addEventListener("DOMContentLoaded", async function() {
    await getImages();
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

let logout = document.getElementById("logout")
logout.addEventListener("click", function() {

    window.location = "index.html";

});

let cardContainer = document.getElementById('cardsContainer');


let botonGuardar = document.getElementById("guardarImg");
botonGuardar.addEventListener("click", async function() {


    let formData = new FormData();

    let totalFiles = document.querySelector(".drop-zone__input").files.length;

    for (let index = 0; index < totalFiles; index++) {

        formData.append('files[]', document.querySelector(".drop-zone__input").files[index]);

    }


    if (!formData.get("files[]")) {

        Swal.fire({
            icon: 'error',
            title: '!Algo ha salido mal!',
            text: 'Asegurate de agregar imagenes antes de subirlas ;)',
        })

    } else {

        progressBar.removeAttribute("style");
        let apiFix = urlBase + '/saveFiles';
        await $.ajax({
            method: "POST",
            url: apiFix,
            data: formData,
            dataType: 'json',
            headers: headers,
            processData: false,
            contentType: false
        }).done(function(response) {

            Swal.fire({
                icon: 'success',
                title: '¡Exito!',
                text: 'Sus Archivos han sido cargado con exito :D',
                confirmButtonText: `Aceptar`,
            }).then(function(result){
                if(result.isConfirmed){

                    location.reload();

                }
            });

            progressBar.setAttribute("style", "visibility : hidden;")
        });
        // cleanPromt();
        // cleanCards();
        // getImages();

    }

});


function cleanPromt() {
    /*Limpia area de arrastre*/
    let dropZone = document.getElementById("files-drop")
    let fileContainer = dropZone.getElementsByTagName("div");
    while (fileContainer[0]) {

        fileContainer[0].parentNode.removeChild(fileContainer[0]);

    }

    const span = document.createElement("span");
    span.setAttribute("class", "drop-zone__prompt");
    span.textContent = "Arrastra Archivos aquí o da Clic para Añadir";

    dropZone.insertAdjacentElement("afterbegin", span);
}

function cleanCards() {


    /*Limpia el contenedor de las cards de imagenes*/
    let cardContainer = document.getElementById("cardsContainer");
    let fileCardsContainer = cardContainer.getElementsByTagName("div");
    while (fileCardsContainer[0]) {

        fileCardsContainer[0].parentNode.removeChild(fileCardsContainer[0]);
    }
}

document.querySelectorAll(".drop-zone__input").forEach(inputElement => {

    const dropZoneElement = inputElement.closest(".drop-zone");

    dropZoneElement.addEventListener("click", e => {
        inputElement.click();
    });

    inputElement.addEventListener("change", e => {

        console.log(typeof(inputElement.files));

        if (inputElement.files.length) {


            Object.entries(inputElement.files).forEach(([key, value]) => {

                updateThumbnail(dropZoneElement, inputElement.files[key]);
            });
        }

    });

    dropZoneElement.addEventListener("dragover", e => {
        e.preventDefault();
        dropZoneElement.classList.add("drop-zone--over");
    });

    ["dragleave", "dragend"].forEach(type => {
        dropZoneElement.addEventListener(type, e => {
            dropZoneElement.classList.remove("drop-zone--over");
        });
    });

    dropZoneElement.addEventListener("drop", e => {
        e.preventDefault();
        if (e.dataTransfer.files.length) {

            inputElement.files = e.dataTransfer.files;

            Object.entries(e.dataTransfer.files).forEach(([key, value]) => {

                updateThumbnail(dropZoneElement, e.dataTransfer.files[key]);

            });
        }

        dropZoneElement.classList.remove("drop-zone--over")

    });

});

function updateThumbnail(dropZoneElement, file) {

    let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

    if (dropZoneElement.querySelector(".drop-zone__prompt")) {

        dropZoneElement.querySelector(".drop-zone__prompt").remove();

    }

    if (!thumbnailElement) {

        thumbnailElement = document.createElement("div");
        thumbnailElement.classList.add("drop-zone__thumb");
        dropZoneElement.appendChild(thumbnailElement);

    } else {

        thumbnailElement = document.createElement("div");
        thumbnailElement.classList.add("drop-zone__thumb");
        dropZoneElement.appendChild(thumbnailElement);

    }

    thumbnailElement.dataset.label = file.name;


    //mostrar descripcion para los archivos imagen
    if (file.type.startsWith("image/")) {

        const reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = () => {

            thumbnailElement.style.backgroundImage = `url('${reader.result}')`

        };

    }
}


async function getImages() {

    let apiFix = urlBase +  `/getImages?tipoRecurso=${tipoRecurso}`;
    let data;

    await $.ajax({
        method: "GET",
        url: apiFix,
        headers: headers,
        processData: false,
        contentType: false,
    }).done(function(response) {

        data = Object.values(response.data)

    });

    if(data.length > 0){

        if(data[0]['TipoRecurso'] == 1){

            Object.entries(data).forEach(([key, value]) => {

                let idFile = value['Id'];
                let srcFile = value['Path'];
                let itemCard = `<div class="card">
                <div class="card-image">
                <img class="materialboxed" src="https://${srcFile}">
            </div>
                    <div class="card-action">
                        <a class="btn-floating halfway-fab waves-effect waves-light green darken-4" id="${idFile}"><i class="material-icons">delete</i></a>
                    </div>
                </div>`;

                cardContainer.insertAdjacentHTML('beforeend', itemCard)

            });
        }else{

            Object.entries(data).forEach(([key, value])=>{
                let idFile = value['Id'];
                let srcFile = value['Path'];
                let itemCard = ``



            });


        }
    }    
    inicializateMaterialBox();
}


function inicializateMaterialBox() {

    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);

    getButtonsData();
}


function getButtonsData() {

    const botones = document.querySelectorAll(".btn-floating");

    botones.forEach(boton => {



        boton.addEventListener("click", function() {

            idFile = this.id;
            // console.log("Se ha dado click a: " + idFile);
            Swal.fire({
                icon: 'warning',
                title: '¿Realmente quieres eliminar este recurso?',
                showCancelButton: true,
                confirmButtonText: `Aceptar`,
            }).then(async function(result) {
                if (result.isConfirmed) {

                    await deleteFile(idFile);
                    // Swal.fire('Saved!', '', 'success')
                    cleanCards();
                    getImages();
                }
            })

        })
    })
}

let pdfFile = document.querySelector(".clicked");
console.log(pdfFile);
    pdfFile.addEventListener("click", function() {

        console.log("Se ha dado click al embed");

    });
async function deleteFile(idFile) {

    let apiFix = urlBase + '/deleteFile';
    let data;
    await $.ajax({
        method: "POST",
        url: apiFix,
        data: {
            "Estado": "I",
            "Id": idFile
        },
        dataType: "json",
        headers: headers,
    }).done(function(response) {
        data = response;
    });
}