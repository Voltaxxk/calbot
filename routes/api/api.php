<?php

use App\Controllers\TopicosController;
use Illuminate\Routing\Router;

$router->group(['namespace' => 'App\Controllers', 'prefix' => 'api'], function (Router $router) {
    $router->get('/', function () {
        return response(['success' => true, 'message' => 'API WORKS']);
    });

    $router->group(['prefix' => 'bot'], function (Router $router){ 
        $router->get('/', ['uses' => 'BotController@index']);
        $router->get('/getBots', ['uses' => 'BotController@show']);
        $router->get('/getPlanes', ['uses' => 'BotController@showPlans']);
        $router->post('/registrarUsuario', ['uses' => 'BotController@storeUser']);
        $router->post('/registrarConversacionGlobal', ['uses' => 'BotController@storeGlobalConversation']);
        $router->post('/registrarConversacionTopico', ['uses' => 'BotController@storeTopicConversation']);
        $router->post('/registrarConversacionEncuesta', ['uses' => 'BotController@storeConversationSurvey']);
        $router->post('/registrarActualizacionDatos', ['uses' => 'BotController@storeUpdateInfo']);
        $router->post('/existeMensaje', ['uses' => 'MensajesRecibidosController@searchAndStoreMessage']);
        $router->get('/despedir/{id}', ['uses' => 'BotController@despedida']);
        $router->post('/registrarActividadUsuario', ['uses' => 'BotController@storeUserRecord']);
        $router->post('/registrarActividadTopico', ['uses' => 'BotController@storeTopicRecord']);
        $router->post('/agencias', ['uses' => 'BotController@showAgencias']);
        $router->post('/actividad', ['uses' => 'BotController@registerActivity']);
        $router->post('/cambios', ['uses' => 'BotController@verifyChange']);
    });

    $router->group(['prefix' => 'respaldoBot'], function (Router $router){ 
        $router->get('/', ['uses' => 'RespaldoBotController@index']);
        $router->get('/getBots', ['uses' => 'RespaldoBotController@show']);
        $router->get('/saludar/{id}', ['uses' => 'RespaldoBotController@saludos']);
        $router->get('/despedir/{id}', ['uses' => 'RespaldoBotController@despedida']);
    });
    
    $router->group(['prefix' => 'topicos'], function (Router $router){ 
        $router->get('/', ['uses' => 'TopicosController@index']);
        $router->get('/{id}', ['uses' => 'TopicosController@show']);
        $router->get('/data/{id}', ['uses' => 'TopicosController@data']);
        $router->post('/newTopic', ['uses' => 'TopicosController@store']);
        $router->post('/editTopic', ['uses' => 'TopicosController@update']);
    });

    $router->group(['prefix' => 'saludos'], function (Router $router){
        $router->post('/newSalute', ['uses' => 'SaludosController@store']);
        $router->post('/editSalute', ['uses' => 'SaludosController@update']);
    });

    $router->group(['prefix' => 'despedidas'], function (Router $router){
        $router->post('/newFarewell', ['uses' => 'DespedidasController@store']);
        $router->post('/editFarewell', ['uses' => 'DespedidasController@update']);
    });

    $router->group(['prefix' => 'contestaciones'], function (Router $router){
        $router->post('/newAnswers', ['uses' => 'ContestacionesController@store']);
        $router->post('/editAnswers', ['uses' => 'ContestacionesController@update']);
    });

    $router->group(['prefix' => 'keyWordOne'], function (Router $router){
        $router->post('/newKeyWordOne', ['uses' => 'KeyWordOne@store']);
        $router->post('/editKeyWordOne', ['uses' => 'KeyWordOne@update']);
    });

    $router->group(['prefix' => 'keyWordTwo'], function (Router $router){
        $router->post('/newKeyWordTwo', ['uses' => 'KeyWordTwo@store']);
        $router->post('/editKeyWordTwo', ['uses' => 'KeyWordTwo@update']);
    });

    $router->group(['prefix' => 'botnuevo'], function (Router $router){ 
        $router->get('/', ['uses' => 'UsuariosController@index']);
    });
    
    $router->group(['prefix' => 'reportes'], function (Router $router){
        $router->get('/', ['uses' => 'BotReports@index']);
        $router->get('/globalBotTime', ['uses' => 'BotReports@globalBotTime']);
        $router->get('/billDetails', ['uses' => 'BotReports@billDetails']);
        $router->get('/serviceProblems', ['uses' => 'BotReports@serviceProblems']);
        $router->get('/productsDescription', ['uses' => 'BotReports@productsDescription']);
        $router->get('/updateInfo', ['uses' => 'BotReports@updateInfo']);
        $router->get('/instructive', ['uses' => 'BotReports@instructive']);
        $router->get('/changeServices', ['uses' => 'BotReports@changeServices']);
        $router->get('/xTrimHbo', ['uses' => 'BotReports@xTrimHbo']);
    });

    $router->group(['prefix' => 'reportesTele'], function (Router $router){
        $router->get('/', ['uses' => 'BotReportsTele@index']);
        $router->get('/totalUse' , ['uses' => 'BotReportsTele@totalUse']);
        $router->get('/totalUseF' , ['uses' => 'BotReportsTele@totalUseFecha']);
        $router->get('/totalAsesores', ['uses' => 'BotReportsTele@totalAsesores']);
        $router->get('/totalAsesoresF', ['uses' => 'BotReportsTele@totalAsesoresFecha']);
        $router->get('/totalVentas', ['uses' => 'BotReportsTele@topicoVenta']);
        $router->get('/totalVentasF', ['uses' => 'BotReportsTele@topicoVentasFecha']);
        $router->get('/totalTv', ['uses' => 'BotReportsTele@topicoTv']);
        $router->get('/totalTvF', ['uses' => 'BotReportsTele@topicoTvFecha']);
        $router->get('/totalInternet', ['uses' => 'BotReportsTele@topicoInternet']);
        $router->get('/totalInternetF', ['uses' => 'BotReportsTele@topicoInternetFecha']);
        $router->get('/totalTvInternet', ['uses' => 'BotReportsTele@topicoTvInternet']); 
        $router->get('/totalTvInternetF', ['uses' => 'BotReportsTele@topicoTvInternetFecha']); 
    });


    $router->group(['prefix' => 'adminBot'], function(Router $router){
        $router->get('/', ['uses' => 'AdminPlanes@index']);
        $router->get('/servicios', ['uses' => 'AdminPlanes@servicios']);
        $router->post('/nuevoServicio', ['uses' => 'AdminPlanes@nuevoServicio']);
        $router->post('/editServicio', ['uses' => 'AdminPlanes@editServicio']);
        $router->get('/planesServicios', ['uses' => 'AdminPlanes@planesServicios']);
        $router->get('/ciudades', ['uses' => 'AdminPlanes@ciudades']);
        $router->get('/agenciasCiudades', ['uses' => 'AdminPlanes@agenciasCiudades']);
        $router->post('/nuevaAgencia', ['uses' => 'AdminPlanes@nuevaAgencia']);
        $router->post('/editAgencias', ['uses' => 'AdminPlanes@editAgencia']);
        $router->get('/recursos', ['uses' => 'AdminPlanes@recursos']);
    });


    $router->group(['prefix' => 'mail'], function(Router $router){
        $router->get('/', ['uses' => 'MailController@index']);
        $router->post('/sendMail', ['uses' => 'MailController@sendMail']);
    });


    $router->group(['prefix' => 'files'], function(Router $router){

        $router->get('/', ['uses' => 'FilesController@index']);
        $router->get('/getImages', ['uses' => 'FilesController@getImages']);
        $router->post('/saveFiles', ['uses' => 'FilesController@saveFiles']);
        $router->post('/deleteFile', ['uses' => 'FilesController@deleteFile']);
    });

    $router->group(['prefix' => 'reports'], function(Router $router){

        $router->get('/atencionesBotAsesores', ['uses' => 'ReportsController@atencionesBotAsesores']);
        $router->get('/annualReport', ['uses' => 'ReportsController@annualReport']);
        $router->get('/accumulatedAnnual', ['uses' => 'ReportsController@accumulatedAnnual']);
        $router->get('/reportMonthByHours', ['uses' => 'ReportsController@reportMonthByHours']);

    });
});
