<?php

use Illuminate\Http\Request;

/** @var $router Router */

$router->name('home')->get('/', function (Request $request) {
    return response('Api Works!');
});

require_once 'routes/api/api.php';

$router->any('{any}', function () {
    return notFound('Route not found');
})->where('any', '(.*)');

$router->options('{any}', function() {
    return ok('');
})->where('any', '(.*)');
